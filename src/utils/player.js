const discordUtils = require('./discord');
const swapi = require('./swapi');
const database = require('../database'); //TODO: Should move this to utils?
const nodeCache = require("node-cache");
const statCalculator = require('swgoh-stat-calc');
const statNamingOption = require("../data/stats.json");
const got = require('got');

const swDataCache = new nodeCache();

const swDataCacheKeys = {
    unitsList: "unitsList",
    gameStatsData: "gameStatsData"
}

module.exports = {
    getUser: async function(userId) {
        const cleanUserId = discordUtils.cleanDiscordId(userId);
        const userRoles = await database.db.any("SELECT * FROM user_guild_role WHERE discord_id = $1", [cleanUserId]);
        const userRegs = await database.db.any('SELECT allycode FROM user_registration WHERE discord_id = $1', [cleanUserId]);

        if (!userRoles && !userRegs) return null;

        const userObj = {
            userId: cleanUserId,
            isBotOwner: discordUtils.isBotOwner(cleanUserId),
            allycodes: new Array(),
            guilds: new Array()
        }

        if (userRoles) {
            for (let ur = 0; ur < userRoles.length; ur++)
            {
                userObj.guilds.push({
                    guildId: userRoles[ur].guild_id,
                    guildAdmin: userRoles[ur].guild_admin,
                    guildBotAdmin: userRoles[ur].guild_bot_admin
                });
            }
        }

        if (userRegs)
        {
            for (let r = 0; r < userRegs.length; r++)
            {
                userObj.allycodes.push(userRegs[r].allycode);
            }
        }

        return userObj;
    },
    getUserRegistration: async function(userId, allycode) {
        const cleanUserId = discordUtils.cleanDiscordId(userId);
        return await database.db.oneOrNone('SELECT * FROM user_registration WHERE discord_id = $1 AND allycode = $2', [cleanUserId, allycode]);
    },
    setupStatCalc: async function () {
        let gameData = swDataCache.get(swDataCacheKeys.gameStatsData);
        if (gameData == undefined)
        {
            gameData = await got('https://swgoh-stat-calc.glitch.me/gameData.json').json();
            swDataCache.set(swDataCacheKeys.gameStatsData, gameData, 86400)
            statCalculator.setGameData( gameData );
        }
    },
    calcRosterStats: async function(roster)
    {
        await this.setupStatCalc();
        let count = statCalculator.calcRosterStats(roster, { gameStyle: true, calcGP: true, language: statNamingOption });
        return count;
    },
    calculateEHP: function(health, protection, armor)
    {
        return Math.round((health + (protection ? protection : 0)) / (1 - armor));
    },
    getUnitsList: async function () {
        var list = swDataCache.get(swDataCacheKeys.unitsList);
        if (list == undefined)
        {
            var match = {
                "rarity":7,
                "obtainable": true,
                "obtainableTime": 0
            };

            let data = await swapi.swapi.fetchData(
                {
                    collection : "unitsList",
                    language: "eng_us",
                    match : match,
                    project :
                        {
                            "nameKey": 1,
                            "baseId": 1,
                            "descKey": 1,
                            "categoryIdList": 1,
                            "combatType": 1
                        }
                }
            );

            list = data.result;
            swDataCache.set(swDataCacheKeys.unitsList, data.result, 86400)
        }
        return list;
    },
    getAllPlayerDataByAllyCode: async function (allycode) {
        return await database.db.any('SELECT * FROM player_data WHERE allycode = $1 ORDER BY timestamp DESC LIMIT 30', [allycode]);
    },
    getAllGoalDataByAllyCode: async function (allycode) {
        return await database.db.any('SELECT * FROM goal_player_status WHERE allycode = $1 ORDER BY timestamp DESC LIMIT 30', [allycode]);
    },
    cleanAndVerifyStringAsAllyCode: function (arg) {
        const allycode = arg.replace(/-/g, "").trim();

        if (allycode.length != 9 || isNaN(allycode))
            throw "Invalid ally code (" + allycode + ").  Needs to be 9 digits."

        return allycode;
    },
    isGuildAdmin: async function(userId, guildId)
    {
        var me = await this.getUser(userId);
        if (!me) return false;

        var guild = me.guilds.find(g => g.guildId == guildId)
        if (!guild || (!guild.guildAdmin && !guild.guildBotAdmin))
            return false;
        
        return true;
    },
}