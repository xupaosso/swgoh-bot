const ApiSwgohHelp = require('api-swgoh-help');
const got = require('got');

module.exports = {
    swapi: new ApiSwgohHelp({
        "username": process.env.API_USERNAME,
        "password": process.env.API_PASSWORD,
    }),
    unitImageCache: {},
    getUnitImage: async function(baseId)
    {
        if (this.unitImageCache[baseId])
            return unitImageCache[baseId];
            
        var image = await got("https://swgoh.gg/game-asset/u/" + baseId + ".png").buffer();
        unitImageCache[baseId] = image;
        return image;
    }
}