const Discord = require('discord.js');
const { MessageEmbed } = require("discord.js");

module.exports = {
    prefix: "^",
    symbols : {
        ok: ":white_check_mark:",
        pass: ":white_check_mark:",
        bad: ":warning:",
        warning: ":warning:",
        fail: ":x:",
        smallBlueDiamond: ":small_blue_diamond:",
        question: ":grey_question:",
        firstPlace: ":first_place:",
        secondPlace: ":second_place:",
        thirdPlace: ":third_place:"
    },
    cleanDiscordId: function (id) {
        if (!id) return null;
        return id.replace(/[^0-9]/g, "");
    },
    isBotOwner: function(userId) {
        let cleanId = this.cleanDiscordId(userId);
        return cleanId == "276185061970149377" || cleanId == "276146474268753920";
    },
    isDiscordTag: function(tag) {
        return /^<(#|@!)[0-9]+>$/g.test(tag);
    },
    isDiscordId: function (id) {
        return /^[0-9]+$/g.test(id);
    },
    makeChannelTaggable: function(id){
        if (!id || id[0] == '<' || !this.isDiscordId(id)) return id;
        return "<#" + id + ">";
    },
    makeIdTaggable: function(id) {
        if (!id || id[0] == '<' || !this.isDiscordId(id)) return id;
        return "<@!" + id + ">";
    },
    splitCommandInput: function(input) {
        if (!input || !input.content) return null;
        let message = input.content;
        if (!message.startsWith(this.prefix)) {
            message = this.prefix+message.split(this.prefix)[1];
        }

        var cmd;

        let spaceIx = message.indexOf(" ");
        if (spaceIx > 0)
        {
            cmd = message.slice(this.prefix.length, spaceIx);
        } else {
            cmd = message.slice(this.prefix.length);
        }
        
        return cmd.split(".");
    },
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    }
}