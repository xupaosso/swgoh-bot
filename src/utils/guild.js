const swapi = require('./swapi');
const database = require('../database');

module.exports = {

    getGuildData: async function(allycode) {
        let payload = {
            allycode:allycode,
            language: "eng_us"
        };

        let { result, error, warning } = await swapi.swapi.fetchGuild(payload);

        if (!result || !result[0]) throw "No guild found for ally code " + allycode;

        let guild = result[0];

        return guild;
    },
    getGuildIdAndAllyCodesForGuildByGuildId: async function (guildId)
    {
        const guildPlayerData = await database.db.any(
            'SELECT allycode,guild_id FROM guild_players WHERE guild_id = $1'
            , [guildId]);

        return { allycodes: guildPlayerData.map(d => d.allycode), guildId: guildPlayerData[0].guild_id };
    },
    getGuildIdAndAllyCodesForGuildByAllycode: async function (allycode) {
        const guildPlayerData = await database.db.any(
            'SELECT allycode,guild_id FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)'
            , [allycode]);

        return { allycodes: guildPlayerData.map(d => d.allycode), guildId: (guildPlayerData.length > 0 ? guildPlayerData[0].guild_id : null) };
    },
    getGuildIdByAllyCode: async function (allycode)
    {
        const guildData = await database.db.any(
            'SELECT guild_id FROM guild_players WHERE allycode = $1', [allycode]
        );

        if (guildData.length == 0)
            throw "Guild not registered for ally code " + allycode;
            
        return guildData[0].guild_id;
    },
    getGuildIdByInput: async function (input)
    {
        // order by link type (alphabetical) -- channel registration overrides server registration
        const guildId = await database.db.oneOrNone("SELECT guild_id FROM guild_discord_link WHERE discord_link = $1 OR discord_link = $2 ORDER BY link_type ASC LIMIT 1", [input.guild.id, input.channel.id], g => g.guild_id)
        return guildId;
    }
}