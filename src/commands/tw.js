const database = require("../database");
const discordUtils = require("../utils/discord.js")
const logger = require('winston');
const twRunPlan = require("./tw/runPlan.js");
const twDeleteTeam = require("./tw/deleteTeam.js");
const twAddTeam = require("./tw/addTeam.js");
const twListTeams = require("./tw/listTeams.js");
const twHelp = require("./tw/help.js");
const twConfig = require("./tw/config");
const twShowPlan = require("./tw/showPlan");
const twSetTeam = require("./tw/setTeam");
const twRemoveTeam = require("./tw/removeTeam");
const twLastPlan = require("./tw/lastPlan");
const twSetDescription = require("./tw/setDescription");
const twMyAssignments = require("./tw/myAssignments");
const twSendAssignments = require("./tw/sendAssignments")
const twExclude = require("./tw/exclude")
const twInclude = require("./tw/include")
const twTestTeam = require("./tw/testTeam")

module.exports = {
    name: "tw",
    aliases: ["tw"],
    description: "TW Planner",
    execute: async function(client, input, args)
    {

        let cmd = discordUtils.splitCommandInput(input);

        if (cmd.length == 1) {
            await twHelp.execute(client, input, args);
            return;
        }

        switch (cmd[1].toLowerCase())
        {
            case "config":
                await twConfig.execute(client, input, args);
                break;
            case "help":
                await twHelp.execute(client, input, args);
                break;
            case "deleteteam":
            case "dt":
                await twDeleteTeam.execute(client, input, args);
                break;
            case "addteam":
            case "at":
                await twAddTeam.execute(client, input, args);
                break;
            case "listteams":
            case "lt":
                await twListTeams.execute(client, input, args);
                break;
            case "runplan":
            case "run":
                await twRunPlan.execute(client, input, args);
                break;
            case "showplan":
            case "sp":
                await twShowPlan.execute(client, input, args);
                break;
            case "setteam":
            case "st":
                await twSetTeam.execute(client, input, args);
                break;
            case "remteam":
            case "rt":
                await twRemoveTeam.execute(client, input, args);
                break;
            case "lastplan":
            case "last":
                await twLastPlan.execute(client, input, args);
                break;
            case "setdescription":
            case "setdesc":
            case "sd":
                await twSetDescription.execute(client, input, args);
                break;
            case "myassignments":
            case "my":
                await twMyAssignments.execute(client, input,args);
                break;
            case "sendassignments":
            case "send":
                await twSendAssignments.execute(client, input, args);
                break;
            case "exclude":
                await twExclude.execute(client, input, args);
                break;
            case "include":
                await twInclude.execute(client, input, args);
                break;
            case "tt":
            case "testteam":
            case "test":
                await twTestTeam.execute(client, input, args);
                break;
            default: break;
        }
    }
}