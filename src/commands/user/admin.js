const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const database = require("../../database");

module.exports = {
    name: "user.admin",
    description: "Toggle admin rights for a user",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }
        
        if (!args)
        {
            input.channel.send("Need to provide a discord tag.");
            return;
        }

        var userIdToMakeAdmin = discordUtils.cleanDiscordId(args[0].toLowerCase().trim());

        var r = await database.db.oneOrNone("UPDATE user_guild_role SET guild_admin = NOT guild_admin WHERE discord_id = $1 AND guild_id = $2 RETURNING guild_admin", [userIdToMakeAdmin, guildId], r => r.guild_admin);

        var message;

        if (r == null) message = "'" + args[0] + "' is not a valid discord ID for a member of your guild."
        else if (r) message = "User "+ discordUtils.makeIdTaggable(userIdToMakeAdmin) + " is now an admin for your guild.";
        else message = "User "+ discordUtils.makeIdTaggable(userIdToMakeAdmin) + " is no longer an admin for your guild.";
        
        input.channel.send(message);
    }
}