const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const guildUtils = require("../../utils/guild")

module.exports = {
    name: "user.register",
    aliases: ["user.r"],
    description: "Register a user",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }
        
        if (!args)
        {
            input.channel.send("Need to provide an ally code and a discord tag.");
            return;
        }
        var splitArgs = args[0].toLowerCase().trim().split(/ +/);

        if (splitArgs.length != 2)
        {
            input.channel.send("Need to provide an ally code and a discord tag.");
            return;
        }

        var allycode = playerUtils.cleanAndVerifyStringAsAllyCode(splitArgs[0]);
        var userIdToRegister = discordUtils.cleanDiscordId(splitArgs[1]);


        // get guild id for the allycode to register
        var guildPlayerInfo = await database.db.oneOrNone("SELECT guild_id FROM guild_players WHERE allycode = $1", [allycode]);

        // player not part of guild
        if (!guildPlayerInfo)
        {
            input.channel.send("Ally code **" + allycode + "** is not yet confirmed as part of a guild. Please double check the ally code. If it is correct, has your guild been registered?  If not, contact the bot owner.");
            return;
        }

        if (!await playerUtils.isGuildAdmin(input.author.id, guildPlayerInfo.guild_id))
        {
            input.channel.send("Need an administrative role for your guild to do this. Check with your guild leader.\r\nIf you are an administrator, the user you are trying to affect may not be recognized by WookieeBot as part of your guild.  Please try again later.");
            return;
        }

        if (await this.registerUserAndSetDefaultRolesForGuild(userIdToRegister, allycode, guildPlayerInfo.guild_id))
            input.channel.send("Success! Ally code **" + allycode + "** has been registered to " + discordUtils.makeIdTaggable(userIdToRegister) + " and assigned to your guild.");
        else
            input.channel.send("User was already registered.")
        
    },
    registerUserAndSetDefaultRolesForGuild: async function(userId, allycode, guildId)
    {
        const existingUser = await playerUtils.getUser(userId);

        var addRole = false, addReg = false;

        if (!existingUser) {
            // user has never been registered before. assign them to the guild and allycode
            addRole = true;
            addReg = true;
        } 
        else 
        {
            if (!existingUser.guilds.find(g => g.guildId == guildId)) {
                // user wasn't assigned as part of the guild, so give them the default roles
                addRole = true;
            }  

            if (!existingUser.allycodes.find(a => a == allycode))
            {
                // user exists, but they do not have that ally code associated
                addReg = true;
            }
        }

        if (addRole) {
            try {
                await database.db.none("INSERT INTO user_guild_role VALUES ($1, $2, $3, $4)", [userId, guildId, false, false]);
            } catch { addRole = false; }
        }

        if (addReg)
        {
            // only one discord user per ally code
            await database.db.none("DELETE FROM user_registration WHERE allycode = $1", [allycode]);
            await database.db.none("INSERT INTO user_registration VALUES ($1, $2)", [allycode, userId]);
        }

        if (addRole || addReg) return true;
        return false;
    }
}