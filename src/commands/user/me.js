const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const guild = require("../../utils/guild");

module.exports = {
    name: "user.me",
    description: "Show information about yourself",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null) {
            input.channel.send("You are not registered with WookieeBot.");
            return;
        }

        var guilds = await database.db.any("SELECT guild_id, guild_name FROM guild");

        var rolesString = botUser.guilds.map((g, i) => `${i+1}. ${this.getGuildName(guilds, g.guildId)}: ${g.guildBotAdmin ? "Bot Admin, Guild Admin" : g.guildAdmin ? "Guild Admin" : "User"}`).join("\r\n")

        var allycodes = botUser.allycodes.map((a, i) => `${i+1}. ${a}`).join("\r\n");

        input.channel.send(
`You are registered with WookieeBot.
        
Your guilds:
${rolesString}

Your ally codes:
${allycodes}`)
    },
    getGuildName: function(guilds, guildId)
    {
        if (guildId && guilds == null) return `:warning: Guild ${guildId} misconfigured. :warning:`
        var guild = guilds.find(g => g.guild_id == guildId);
        if (!guild) return `:warning: Guild *${guildId}* misconfigured. :warning:`
        return guild.guild_name;
    }
}