const discordUtils = require("../utils/discord.js");
const guildRegister = require("./guild/register.js");
const guildRefresh = require("./guild/refresh.js");
const guildRefreshRoster = require("./guild/refreshRoster.js");
const guildRefreshUlts = require("./guild/refreshUlts.js");
const guildLinkToDiscord = require("./guild/linkToDiscord")
const guildStatus = require("./guild/status")
const guildCache = require("./guild/cache")
const guildTBData = require ("./guild/tbData")

module.exports = {
    name: "guild",
    description: "Gather data for the guild",
    execute: async function(client, input, args) {

        
        let cmd = discordUtils.splitCommandInput(input);

        switch (cmd[1])
        {
            case "register":
                await guildRegister.execute(client, input, args);
                break;
            case "refresh":
                await guildRefresh.execute(client, input, args);
                break;
            case "refreshroster":
                await guildRefreshRoster.execute(client, input, args);
                break;
            case "refreshults":
                await guildRefreshUlts.execute(client, input, args);
                break;
            case "addserver":
                await guildLinkToDiscord.addServer(client, input, args);
                break;
            case "addchannel":
                await guildLinkToDiscord.addChannel(client, input, args);
                break;
            case "status":
                await guildStatus.execute(client, input, args);
                break;
            case "cache":
                await guildCache.execute(client, input, guildCache.types.all);
                break;
            case "cacheults":
                await guildCache.execute(client, input, guildCache.types.ultsOnly);
                break;
            case "cacherosters":
                await guildCache.execute(client, input, guildCache.types.rostersOnly);
                break;
            case "cacheone":
                await guildCache.execute(client, input, guildCache.types.one, args);
                break;
            case "uploadtbdata":
                await guildTBData.upload(client, input, args);
                break;
            case "deletetbdata":
                await guildTBData.delete(client, input, args);
                break;
            case "listtbdata":
                await guildTBData.listTBData(client, input, args);
                break;
            case "addmanualtbdata":
                await guildTBData.addManualTbData(client, input, args);
                break;
            case "deletemanualtbdata":
                await guildTBData.deleteManualTbData(client, input, args);
                break;
            default: break;
        }
    }
}