const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const guildRefresh = require("./refresh.js");
const { MessageEmbed } = require("discord.js");


module.exports = {
    name: "guild.status",
    description: "Check registration status for a guild.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        var guildId = await guildUtils.getGuildIdByInput(input);

        const members = await database.db.any(`
        select gp.guild_name, gp.allycode, gp.player_name, ur.discord_id, ugr.guild_admin, ugr.guild_bot_admin
        from guild_players gp
        left join user_registration ur on gp.allycode = ur.allycode 
        left join user_guild_role ugr on ur.discord_id = ugr.discord_id and gp.guild_id = ugr.guild_id
        where gp.guild_id = $1`, [guildId]);

        if (members.length == 0) {
            input.channel.send("Your guild is not registered with WookieeBot :cry:");
            return;
        }
        var guildName = members[0].guild_name;
        var embedDescription = "";

        for (var m in members)
        {
            var member = members[m];
            var allycode = member.allycode;
            var name = member.player_name;
            var discordId = discordUtils.makeIdTaggable(member.discord_id);
            
            embedDescription += name + " (" + allycode + "): " + (discordId ? discordId : ":warning: Not Registered") + (member.guild_admin ? " :crown:" : "") + "\r\n"
        }

        var embedToSend = new MessageEmbed()
            .setTitle("Guild registration status for **" + guildName + "**:")
            .setDescription(embedDescription)
            .setColor(0xD2691E);

        input.channel.send(embedToSend);
    }
    
}