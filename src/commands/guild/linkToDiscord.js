const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const guildRefresh = require("./refresh.js");

const linkType = {
    server: "server",
    channel: "channel"
}

module.exports = {
    addServer: async function(client, input, args) {
        this.addLink(input, linkType.server, args);
    },
    addChannel: async function(client, input, args) {
        this.addLink(input, linkType.channel, args);
    },
    addLink: async function(input, type, args)
    {
        var botUser = await playerUtils.getUser(input.author.id);

        var guildIndex = 1;
        if (args)
        {
            var arg = args[0].trim();
            if (args[0].indexOf("/") == 0)
            {

                guildIndex = parseInt(args[0].slice(1));
            }
        }

        var linkId = null;
        if (type == linkType.server)
            linkId = input.guild.id;
        else
            linkId = input.channel.id;

        if (isNaN(guildIndex) || !botUser.guilds[guildIndex-1])
        {
            input.channel.send("Invalid alt reference.  Use user.me to check your registered guilds.")
            return;
        }

        if (!botUser.guilds[guildIndex-1].guildBotAdmin)
        {
            input.channel.send("You must be a bot admin for your guild to do this.")
            return;
        }

        var guildId = botUser.guilds[guildIndex-1].guildId;

        try {
            await database.db.any("INSERT INTO guild_discord_link (guild_id, discord_link, link_type) VALUES ($1, $2, $3)", [guildId, linkId, type]);
        } catch (error)
        {
            if (error.constraint == "discord_link_unique")
                input.channel.send((type == linkType.server ? "Server" : "Channel") + " already registered.")
            else
                input.channel.send("An error occurred.  Please try again or contact the bot owner.")
            return;
        }
        input.channel.send(
            (type == linkType.server ? "Server" : "Channel") + " registered to your guild.  Commands run on this server will now refer to your guild.");

    }
    
}