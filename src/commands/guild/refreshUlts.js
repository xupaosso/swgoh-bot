const guildUtils = require("../../utils/guild.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
var logger = require('winston');
const got = require('got');

module.exports = {
    name: "guild.refreshults",
    description: "Refresh ultimate data for the guild",
    execute: async function(client, input, args) {
        
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        var allycode;
        try {
            allycode = await database.db.one("SELECT allycode FROM guild WHERE guild_id = $1", [guildId], a => a.allycode)
        } catch {
            input.channel.send("Your guild doesn't seem to be properly registered.  Contact the bot developer.");
            return;
        }
        const feedbackMessage = await input.channel.send("Requesting fresh ultimate data for your guild...")
        await this.loadUltDataByAllycode(allycode);
        feedbackMessage.edit("Updated all ultimate data for your guild.")
    },
    fetchUltDataFromAPIAsDbData: async function(allycodes)
    {
        var ultDataInsertValues = new Array();
        let errors = new Array();
        
        var promises = new Array();
        
        for (var gpIx in allycodes) {
            var allycode = allycodes[gpIx];

            promises.push(this.fetchUltDataFromApiForOneAllycode(allycode));
        }

        let allUltUnits = await Promise.all(promises);


        for (var u = 0; u < allUltUnits.length; u++)
        {
            if (allUltUnits[u].error)
            {
                errors.push(allUltUnits[u].error);
                continue;
            }

            if (allUltUnits[u].ultUnits == null) continue;

            for (var u2 = 0; u2 < allUltUnits[u].ultUnits.length; u2++)
            {
                let baseId = allUltUnits[u].ultUnits[u2].definitionId.slice(0, allUltUnits[u].ultUnits[u2].definitionId.indexOf(":"));

                ultDataInsertValues.push({
                    allycode: allUltUnits[u].allycode,
                    unit_base_id: baseId,
                    has_ult: true
                });
            }
        }

        return { ultDataInsertValues, errors };
    },
    fetchUltDataFromApiForOneAllycode: async function(allycode)
    {
        let result;
        try {
            result = await got("https://swgoh.shittybots.me/api/player/" + allycode, {headers: { "shittybot":"YXBpLjE2MDcwMTEwMjA3NjYuWTJscGFXUnkuc3dnb2g=" }})
            let jsonRes = JSON.parse(result.body);
            let ultUnits = jsonRes.rosterUnitList.filter(u => u.purchasedAbilityIdList.length > 0)
            return { allycode, ultUnits, error: null };
        }
        catch (error)
        {
            return { allycode, ultUnits: null, error };
        }
    },
    saveUltDataForAllyCodes: async function(allycodes)
    {
        const ultDataCS = new database.pgp.helpers.ColumnSet(
            ['allycode', 
            'unit_base_id', 
            'has_ult'], 
            {table: 'player_ult'});
            
        let { ultDataInsertValues, errors } = await this.fetchUltDataFromAPIAsDbData(allycodes);
        
        const ultDataInsert = database.pgp.helpers.insert(ultDataInsertValues, ultDataCS);

        // delete old ult data
        await database.db.none("DELETE FROM player_ult WHERE allycode LIKE ANY ($1)", [allycodes.map(a => a.toString())]);

        // insert new ult data
        await database.db.none(ultDataInsert);
    },
    loadUltDataByAllycode: async function(allycode)
    {
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
        await this.saveUltDataForAllyCodes(guildPlayers.allycodes);
    }
}