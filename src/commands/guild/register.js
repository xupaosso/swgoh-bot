const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const guildRefresh = require("./refresh.js");

module.exports = {
    name: "guild.register",
    description: "Register a guild for data tracking",
    execute: async function(client, input, args) {
        
        // for now, bot admin only
        if (!discordUtils.isBotOwner(input.author.id)) return;
        
        if (!args)
        {
            input.channel.send("Need to provide an ally code.");
            return;
        }
    
        let allycode;
        try {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
        } catch (e)
        {
            input.channel.send(e);
            return;
        }

        var guild = await guildUtils.getGuildData(allycode);
        
        const existingGuild = await database.db.one('SELECT COUNT(*) FROM guild WHERE guild_id = $1', [guild.id], r => r.count > 0);
    
        if (!existingGuild)
        {
            let guildPlayersAndDataMessage = await this.createGuildEntryAndPopulateData(input, guild, allycode);
            
            input.channel.send("Guild **" + guild.name + "** registered.\r\n" + guildPlayersAndDataMessage);
        }
        else {
            input.channel.send("Guild **" + guild.name + "** is already registered.");
        }

        // initialize TW planner settings
        const twMetadata = await database.db.oneOrNone("SELECT guild_id FROM tw_metadata WHERE guild_id = $1", guild.id);
        if (!twMetadata)
        {
            try {
                await this.setupTwMetadata(guild);
            } catch (e)
            {
                input.channel.send("Error initializing TW settings: " + (e.message ? e.message : e));
                return;
            }
            input.channel.send("Initialized TW settings.  Check with **tw.config**.");
        }
    },
    setupTwMetadata: async function(guild) 
    {
        await database.db.any("INSERT INTO tw_metadata (max_teams_per_territory, default_max_squads_per_player, default_max_fleets_per_player, guild_id) VALUES ($1, $2, $3, $4)",
        [25, 5, 1, guild.id])
    },
    createGuildEntryAndPopulateData : async function(input, guild, allycode)
    {
        // records to be updated:
        var guildInsertData = new Array();
        
        // declare ColumnSet once, and then reuse it:
        const guildCS = new database.pgp.helpers.ColumnSet(['guild_name', 'guild_id', 'allycode'], {table: 'guild'});
    
        guildInsertData.push({
            guild_name: guild.name,
            guild_id: guild.id,
            allycode: allycode
        });
        
        const guildInsert = database.pgp.helpers.insert(guildInsertData, guildCS);
        
        // executing the query:
        await database.db.none(guildInsert);
    
        // refresh guild data
        return await guildRefresh.populateGuildPlayersAndData(input, guild);
    }
    
}