const guildUtils = require("../../utils/guild.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const refreshUlts = require("./refreshUlts");
var logger = require('winston');
const { MessageEmbed } = require("discord.js");
const guild = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord")

module.exports = {
    name: "guild.refreshroster",
    description: "Refresh roster for the guild (not data)",
    execute: async function(client, input, args) {
        
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        const message = await input.channel.send(`Refreshing guild data for your guild.`)
    

        var allycode;
        try {
            allycode = await database.db.one("SELECT allycode FROM guild WHERE guild_id = $1", [guildId], a => a.allycode)
        } catch {
            input.channel.send("Your guild doesn't seem to be properly registered.  Contact the bot developer.");
            return;
        }

        var guild;

        try {
            guild = await guildUtils.getGuildData(allycode);
        }
        catch (e)
        {
            input.channel.send(this.createErrorEmbed("Error", e));
            return;
        }

        if (!guild || guild.roster.length == 0)
        {
            input.channel.send(this.createErrorEmbed("Error", "Guild seems to have no members for ally code " + allycode + "."));
            return;
        }

        message.edit(`Found guild **${guild.name}** with ${guild.roster.length} members.`)
        try {
            await this.populateGuildPlayers(guild);
            message.edit(`Updated guild roster for guild **${guild.name}**.  Found ${guild.roster.length} members.`)
        } catch (error) {
            let errorMsg = `Failed to populate Guild players for guild **${guild.name}**. Error: ${error}`;
            logger.log(errorMsg)
            message.edit(errorMsg)
            return;
        }
    },
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    },
    populateGuildPlayers: async function(guild)
    {
        if (!guild) return;

        await this.refreshGuildPlayers(guild);
    },
    refreshGuildPlayers: async function (guild)
    {
        var guildId = guild.id;

        // records to be updated:
        var guildPlayersInsertData = new Array();

        // declare ColumnSet once, and then reuse it:
        const guildPlayersCS = new database.pgp.helpers.ColumnSet(['guild_name', 'guild_id', 'player_name', 'allycode'], {table: 'guild_players'});

        for (var m = 0; m < guild.roster.length; m++)
        {
            var memberAllyCode = guild.roster[m].allyCode;
            guildPlayersInsertData.push({
                guild_name: guild.name,
                guild_id: guildId,
                player_name: guild.roster[m].name,
                allycode: memberAllyCode
            });
        }
        
        const guildPlayersInsert = database.pgp.helpers.insert(guildPlayersInsertData, guildPlayersCS);
        
        // clear out old guild data
        await database.db.none("DELETE FROM guild_players WHERE guild_id = $1", [guildId]);
        
        // executing the query:
        await database.db.none(guildPlayersInsert);
    }
}