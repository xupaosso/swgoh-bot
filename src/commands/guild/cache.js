const guildUtils = require("../../utils/guild.js");
const playerUtils = require("../../utils/player.js");
const discordUtils = require("../../utils/discord");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
var logger = require('winston');
const guild = require("../../utils/guild.js");
const refreshUlts = require("./refreshUlts");
const { MessageEmbed } = require("discord.js");

module.exports = {
    name: "guild.cache",
    description: "Cache roster for the guild",
    types: {
        all: 1,
        rostersOnly: 2,
        ultsOnly: 3,
        one: 4
    },
    execute: async function(client, input, type, args) {
        
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        var allycode, guildAllycodes;

        if (type == this.types.one && args && args.length > 0)
        {
            try {
                allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
                guildAllycodes = [allycode];
            } catch (error)
            {
                input.channel.send(discordUtils.createErrorEmbed("Error", error));
                return;
            }
        }
        else {
            try {
                allycode = await database.db.one("SELECT allycode FROM guild WHERE guild_id = $1", [guildId], a => a.allycode)
            } catch {
                input.channel.send("Your guild doesn't seem to be properly registered.  Contact the bot developer.");
                return;
            }

            var feedbackMessage = await input.channel.send("Checking guild roster...");

            var guild;
            
            try {
                guild = await guildUtils.getGuildData(allycode);
            }
            catch (e)
            {
                input.channel.send(this.createErrorEmbed("Error", e));
                return;
            }

            feedbackMessage.edit("Found " + guild.roster.length + " players. Caching data...")

            guildAllycodes = guild.roster.map(r => r.allyCode);
        }


        if (type == this.types.all || type == this.types.rostersOnly || type == this.types.one)
        {
            var promises = new Array();
            var maxCodesPerRequest = 10;
            var totalGuildMembers = guildAllycodes.length;
            var totalRequests = Math.ceil(totalGuildMembers/maxCodesPerRequest);

            for (var m = 0; m < totalRequests; m++)
            {
                var allycodes;
                if (m != totalRequests-1)
                    allycodes = guildAllycodes.slice(m*maxCodesPerRequest, (m+1)*maxCodesPerRequest);
                else
                    allycodes = guildAllycodes.slice(m*maxCodesPerRequest);

                promises.push(
                    this.cacheRosters(allycodes, guildId)
                )
            }

            let failuresLists = await Promise.all(promises);

            var failuresCount = 0;
            var failures = "";
            for (var failuresListIx in failuresLists)
            {
                var fails = failuresLists[failuresListIx];
                if (fails.length == 0) continue;

                for (var f in fails)
                {

                    if (fails[f].message == null) continue;

                    failures += fails[f].message + "\r\n";
                    failuresCount++;
                    guildAllycodes = guildAllycodes.filter(a => a != fails[f].allycode)
                }
            }
            

            var embedToSend = new MessageEmbed().setTitle("Guild Cache Results")
            var messageToSend = "Cached " + (totalGuildMembers - failuresCount) + " players.";
            if (failuresCount > 0)
            {
                if (failuresCount == 1) messageToSend += "\r\n\r\nThere was 1 failure\r\n" + failures;
                else messageToSend += "\r\n\r\nThere were " + (failuresCount) + " failures:\r\n" + failures;
                embedToSend.setColor(0xaa0000);
            } 
            else
            {
                embedToSend.setColor(0x00aa00);
            }

            embedToSend.setDescription(messageToSend);
            await input.channel.send(embedToSend);
        }

        if (guildAllycodes.length > 0 && (type == this.types.all || type == this.types.ultsOnly || type == this.types.one))
        {
            input.channel.send("Attempting to get and cache latest data about ultimate abilities...")
            
            failures = "";
            var ultFail = 0;
            var ultSuccess = 0;
            try {
                let {ultDataInsertValues, errors } = await refreshUlts.fetchUltDataFromAPIAsDbData(guildAllycodes);
                for (var gac = 0; gac < guildAllycodes.length; gac++)
                {
                    let ac = guildAllycodes[gac];

                    let ults = ultDataInsertValues.filter(d => d.allycode == ac);
                    if (ults.length > 0) {
                        await database.db.any("UPDATE player_cache SET ult_data_json = $1 WHERE allycode = $2 AND guild_id = $3",
                            [JSON.stringify(ults), ac.toString(), guildId])
                    }

                    ultSuccess++;
                }

                for (var e in errors)
                {
                    failures += "Error loading ultimate data: " + e.message + "\r\n";
                    ultFail++;
                }
            } catch (e)
            {
                failures += "Error loading ultimate data: " + (e.message ? e.message : e) + "\r\n";
                ultFail = 1;
            }

            messageToSend = `Cached ultimate data for ${ultSuccess} player${ultSuccess == 1 ? `` : `s`}.`;
            let color = 0x00aa00;
            if (ultFail > 0)
            {
                color = 0xaa0000;
                messageToSend += `\r\nSomething went wrong:\r\n${failures}`
            }
            
            let embedToSend = new MessageEmbed().setTitle("Guild Ultimate Cache Results")
                .setDescription(messageToSend)
                .setColor(color);

            input.channel.send(embedToSend)
        }
    },
    cacheRosters: async function(allycodes, guildId)
    {
        var errors = new Array();
        try {
            var timestamp = new Date();
            let payload = {
                allycode: allycodes,
                language: "eng_us"
            };

            let playerDatas = await swapiUtils.swapi.fetchPlayer(payload);

            if (playerDatas.result)
            {
                for (var d = 0; d < playerDatas.result.length; d++)
                {
                    let errorMessage;
                    // if (playerDatas.result[d].guildRefId != guildId)
                    //     errorMessage = `Player **${playerDatas.result[d].name} (${playerDatas.result[d].allyCode})** is not recognized as a member of your guild. Ignored.`;
                    // else 
                    errorMessage = await this.saveDataToDb(playerDatas.result[d], timestamp, guildId)

                    if (errorMessage)
                    {
                        errors.push({ allycode: playerDatas.result[d].allyCode, message: errorMessage });
                    }
                }
            }
            else {
                for (var a in allycodes)
                {
                    let msg = `Could not retrieve data for ${allycodes[a]}.${playerDatas.error ? `Error: ${playerDatas.error}` : ``}`
                    errors.push({ allycode: a, message: msg });
                }
            }
        }
        catch (error) {
            for (var a in allycodes)
            {
                let msg = `Something went wrong getting data for ${allycodes[a]}: ${error}`
                errors.push({ allycode: a, message: msg });
            }
        }

        return errors;
    },
    saveDataToDb: async function(data, timestamp, guildId)
    {
        var dataText = JSON.stringify(data);
        try {
            await database.db.none(`
            INSERT INTO player_cache (allycode, data_json, timestamp, guild_id) VALUES ($1, $2, $3, $4) 
            ON CONFLICT (allycode, guild_id)
            DO UPDATE SET data_json = $2, timestamp = $3`, [data.allyCode, dataText, timestamp, guildId]);
        } catch (error) {
            return "Could not cache player: " + data.allyCode;
        }
        return null;

    },
    createErrorEmbed: function(title, errorMessage) {
        var errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    },
}