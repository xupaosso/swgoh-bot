const guildUtils = require("../../utils/guild.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const refreshUlts = require("./refreshUlts");
const refreshRoster = require("./refreshRoster");
var logger = require('winston');
const { MessageEmbed } = require("discord.js");
const guild = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord")

module.exports = {
    name: "guild.refresh",
    description: "Refresh data for the guild",
    execute: async function(client, input, args) {
        
        // for now, bot admin only
        if (!discordUtils.isBotOwner(input.author.id)) return;
        
        if (!args || !args[0])
        {
            input.channel.send("Need to provide an ally code.");
            return;
        }
    
        var allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
        
        const message = await input.channel.send(`Refreshing guild data for ${allycode}'s guild.`)
    
        var guild;

        try {
            guild = await guildUtils.getGuildData(allycode);
        }
        catch (e)
        {
            input.channel.send(this.createErrorEmbed("Error", e));
            return;
        }

        if (!guild || guild.roster.length == 0)
        {
            input.channel.send(this.createErrorEmbed("Error", "Guild seems to have no members for ally code " + allycode + "."));
            return;
        }

        message.edit(`Found guild **${guild.name}** with ${guild.roster.length} members.`)
        try {
            await refreshRoster.populateGuildPlayers(guild);
            message.edit(`Updated guild roster for guild **${guild.name}**.  Found ${guild.roster.length} members.\r\nPopulating player data...`)
        } catch (error) {
            let errorMsg = `Failed to populate Guild players for guild **${guild.name}**. Error: ${error}`;
            logger.log(errorMsg)
            message.edit(errorMsg)
            return;
        }

        try {
            await this.populatePlayerData(guild);
            message.edit(`Populated player data for guild **${guild.name}**.\r\nPopulating ultimate data...`)
        } catch (error) {
            let errorMsg = `Failed to populate player data for guild **${guild.name}**. Error: ${error}`;
            logger.log(errorMsg)
            message.edit(errorMsg)
            return;
        }

        try {
            await this.populateUltData(guild);
            message.edit(`Populated ult data for guild **${guild.name}**.`)
        } catch (error) {
            let errorMsg = `Failed to populate ultimate data for guild **${guild.name}**. Error: ${error}`;
            logger.log(errorMsg)
            message.edit(errorMsg)
            return;
        }
    },
    refreshGuildData : async function(allycode)
    {
        let guild;
        try {
            guild = await guildUtils.getGuildData(allycode);
        }
        catch (e)
        {
            logger.error("Failed to get guild data for allycode " + allycode + ". Error: " + e);
            return;
        }

        if (!guild) {
            logger.error("No guild data for allycode " + allycode);
            return;
        }

        if (guild.roster.length == 0) {
            logger.error("Guild has no members... odd.  Allycode: " + allycode);
            return;
        }

        await refreshRoster.populateGuildPlayers(guild);
        await this.populatePlayerData(guild);

        // Not sure we really want to do this. Depends on performance, which can sometimes be dismal.
        // TODO: can we improve handling if this service times out or otherwise errors?
        await this.populateUltData(guild);
    },
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    },
    populatePlayerData: async function(guild) {

        if (!guild) return;

        let guildAllyCodes = guild.roster.map(r => r.allyCode);
        await this.insertPlayerDataForAllyCodes(guildAllyCodes);
    },
    populateUltData: async function(guild)
    {
        if (!guild) return;

        let guildAllyCodes = guild.roster.map(r => r.allyCode);

        await refreshUlts.saveUltDataForAllyCodes(guildAllyCodes);
    },
    insertPlayerDataForAllyCodes: async function (allyCodes)
    {
        var updateTime = new Date();
        var result;

        let [result1, result2] = await Promise.all([
            this.fetchPlayerData(allyCodes.slice(0, 25)), 
            this.fetchPlayerData(allyCodes.slice(25))
        ]);

        result = result1.concat(result2);
        
        const playerDataCS = new database.pgp.helpers.ColumnSet(
            ['allycode', 
            'player_name', 
            'gp', 
            'gp_fleet',
            'gp_squad',
            'arena_rank', 
            'fleet_rank',
            'gl_count', 
            'gac_lifetime_score',
            'mod_quality',
            'mod_quality2',
            'mods_six_dot',
            'mods_speed25',
            'mods_speed20',
            'mods_speed15',
            'mods_speed10',
            'timestamp'], 
            {table: 'player_data'});

        const playerDataStaticCS = new database.pgp.helpers.ColumnSet(
            ['allycode', 
            'player_name', 
            'reek_win', 
            'gl_rey',
            'gl_slkr',
            'gl_jml',
            'gl_see',
            'gl_jmk',
            'payout_utc_offset_minutes'
            ], 
            {table: 'player_data_static'});

        var playerDataInsertValues = new Array();
        var playerDataStaticInsertValues = new Array();

        for (var playerIndex = 0; playerIndex < result.length; playerIndex++)
        {
            var player = result[playerIndex];

            var poUTCOffsetMinutes = player.poUTCOffsetMinutes;
            var gp = player.stats[0].value;
            var gpSquad = player.stats[1].value;
            var gpFleet = player.stats[2].value;
            var arenaRank = player.arena.char.rank;
            var fleetRank = player.arena.ship.rank;
            var name = player.name;
            var allyCode = player.allyCode;
            var gacLifetimeScore = player.grandArenaLifeTime;
            var reekWin = player.portraits.unlocked.indexOf("PLAYERPORTRAIT_REEK") >= 0;

            var glCount = 0;
            var glRey = false;
            var glSlkr = false;
            var glJml = false;
            var glSee = false;
            var glJmk = false;
            var modQuality;
            var modCounts = {
                sixDot: 0,
                speed25: 0,
                speed20: 0,
                speed15: 0,
                speed10: 0
            };

            player.roster.forEach(unit => {
                for (var modIndex in unit.mods)
                {
                    var mod = unit.mods[modIndex];
                    if (mod.pips == 6)
                        modCounts.sixDot++;
                    
                    for (var secondaryIndex in mod.secondaryStat)
                    {
                        var secondary = mod.secondaryStat[secondaryIndex];
                        if (secondary.unitStat == 5) // speed
                        {
                            if (secondary.value >= 25) modCounts.speed25++;
                            if (secondary.value >= 20) modCounts.speed20++;
                            if (secondary.value >= 15) modCounts.speed15++;
                            if (secondary.value >= 10) modCounts.speed10++;
                        }
                    }
                }

                for (var skillIndex in unit.skills)
                {
                    // If they are a GL, add count
                    if (unit.skills[skillIndex].nameKey == 'Galactic Legend')
                    {
                        glCount++;
                        if (unit.nameKey == "Rey") glRey = true;
                        else if (unit.nameKey == "Supreme Leader Kylo Ren") glSlkr = true;
                        else if (unit.nameKey == "Jedi Master Luke Skywalker") glJml = true;
                        else if (unit.nameKey == "Sith Eternal Emperor") glSee = true;
                        else if (unit.nameKey == "Jedi Master Kenobi") glJmk = true;
                    }
                }
            });

            // same as DSR bot
            modQuality = modCounts.speed15 / (gpSquad / 100000.0);

            // double counts +20s, triple counts +25s, and incorporates 6dot
            modQuality2 = (modCounts.speed15 + modCounts.speed20 + modCounts.speed25 + 0.5*modCounts.sixDot) / (gpSquad / 100000.0);

            playerDataInsertValues.push({
                allycode: allyCode,
                player_name: name,
                gp: gp,
                gp_fleet: gpFleet,
                gp_squad: gpSquad,
                arena_rank: arenaRank,
                fleet_rank: fleetRank,
                gl_count: glCount,
                gac_lifetime_score: gacLifetimeScore,
                mod_quality: modQuality,
                mod_quality2: modQuality2,
                mods_six_dot: modCounts.sixDot,
                mods_speed25: modCounts.speed25,
                mods_speed20: modCounts.speed20,
                mods_speed15: modCounts.speed15,
                mods_speed10: modCounts.speed10,
                timestamp: updateTime
            });

            playerDataStaticInsertValues.push({
                allycode: allyCode,
                player_name: name,
                gl_rey: glRey,
                gl_slkr: glSlkr,
                gl_jml: glJml,
                gl_see: glSee,
                gl_jmk: glJmk,
                reek_win: reekWin,
                payout_utc_offset_minutes: poUTCOffsetMinutes
            });
        }

        const playerDataInsert = database.pgp.helpers.insert(playerDataInsertValues, playerDataCS);
        const playerDataStaticInsert = database.pgp.helpers.insert(playerDataStaticInsertValues, playerDataStaticCS);

        // delete old static data
        await database.db.none("DELETE FROM player_data_static WHERE allycode LIKE ANY ($1)", [allyCodes.map(a => a.toString())]);

        // refresh static data
        await database.db.none(playerDataStaticInsert);
        
        // insert new dynamic data
        await database.db.none(playerDataInsert);
    },
    fetchPlayerData: async function (allyCodes)
    {
        if (!allyCodes || allyCodes.length == 0) return;
        
        let payload = { 
            allycodes: allyCodes, 
            language: "ENG_US",
            project: {
                poUTCOffsetMinutes: 1,
                stats: 1,
                arena: 1,
                name: 1,
                allyCode: 1,
                grandArenaLifeTime: 1,
                portraits: { unlocked: 1},
                roster: {
                    nameKey: 1,
                    rarity: 1,
                    gear: 1,
                    mods: 1,
                    skills: 1
                }
            } 
        };

        let { result, error, warning } = await swapiUtils.swapi.fetchPlayer(payload);

        if (error)
            throw error;

        return result;
    }
}