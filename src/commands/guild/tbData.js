const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const guildRefresh = require("./refresh.js");
const got = require('got');
const parse = require("csv-parse/lib/sync");
const { MessageEmbed } = require("discord.js");
const https = require('https');

const tbTypes = {
    dsgtb: { key: "DSGTB", maxWaves: 75, maxPhases: 4 },
    lsgtb: { key: "LSGTB", maxWaves: 74, maxPhases: 4 },
    dshtb: { key: "DSHTB", maxWaves: 76, maxPhases: 6 },
    lshtb: { key: "LSHTB", maxWaves: 72, maxPhases: 6 }
}

const dataTypes = {
    waves: 1
}

module.exports = {
    name: "guild.uploadtbdata",
    description: "Upload TB Data from HotBot for tracking",
    upload: async function(client, input, args) {

        let { botUser, guildId } = await this.checkPermissions(input) || {};

        if (guildId == null) return;
        
        var attachment = input.attachments.first();
        if (!attachment)   
        {
            input.channel.send("You must upload a TB data export from HotUtils by running the hotbot command **tbstats all**.");
            return;
        }

        if (attachment.size >= 1024*1024*8) // 8MB limit, maybe?
        {
            input.channel.send("This file is too big.  Please limit to less than 10MB.  If you have a lot of TB data, you may need to split the file into more than one.  Just upload each separately.");
            return;
        }

        // let { tbType, dataDate } = this.parseTBTypeAndDateFromArgs(input, args) || {};
        // if (tbType == null || dataDate == null) return;

        var ssUrl = attachment.url;
        var buffer = await got(ssUrl);
        
        var bufferString = buffer.rawBody.toString("ucs2");
        const records = parse(bufferString, { columns: true });

        var uniqueTBDates = new Array();
        records.filter(r => {
            let type = this.hotbotCodeToTbType(r.Instance);
            return r.MapStatId == `strike_attempt_round_${type.maxPhases}`
        }).forEach(r => {
            var dateMillis = parseInt(r['﻿CurrentRoundEndTime']);
            if (uniqueTBDates.indexOf(dateMillis) == -1) uniqueTBDates.push(dateMillis);
        });

        var totalCombatWavesRecords = records.filter(r => r.MapStatId == "strike_encounter" && uniqueTBDates.indexOf(parseInt(r['﻿CurrentRoundEndTime'])) >= 0);
        if (totalCombatWavesRecords.length == 0)
        {
            input.channel.send("There doesn't seem to be any combat summary data (strike_encounter) in your spreadsheet.  Please double check.");
            return;
        }

        try {
            playerUtils.cleanAndVerifyStringAsAllyCode(totalCombatWavesRecords[0].AllyCode); // throws exception if it's not an ally code

            let tbType = this.hotbotCodeToTbType(totalCombatWavesRecords[0].Instance);
            if (tbType == null)
                throw "There is something wrong with the Instance codes in the file. Please verify you have not altered the file.";

            if (totalCombatWavesRecords[0].Score %1 !== 0 
                || totalCombatWavesRecords[0].Score < 0 
                || totalCombatWavesRecords[0].Score > tbType.maxWaves)
                throw "There seems to be something wrong with the combat mission waves.  These do not appear to be valid numbers."    
        } 
        catch (error)
        {
            input.channel.send(error.message ? error.message : error);
            return;
        }


        const tbDataCS = new database.pgp.helpers.ColumnSet(
            ['allycode', 
            'tb_type', 
            'date', 
            'combat_waves',
            'guild_id'
            ], 
            {table: 'player_tb_data'});
            
        var tbDataInsertValues = new Array();


        totalCombatWavesRecords.forEach(record => {
            var dateMillis = parseInt(record['﻿CurrentRoundEndTime']);
            let date = new Date(dateMillis);

            let tbType = this.hotbotCodeToTbType(record.Instance);

            tbDataInsertValues.push(
                {
                    allycode: record.AllyCode,
                    tb_type: tbType.key,
                    date: date,
                    combat_waves: record.Score,
                    guild_id: guildId
                }
            )
        })

        uniqueTBDates = uniqueTBDates.map(d => new Date(d));
        var recsDeleted = 0;

        try {
            recsDeleted = await database.db.result("DELETE FROM player_tb_data WHERE guild_id = $1 AND manual_entry = FALSE AND date IN ($2:raw)", 
            [
                guildId, 
                uniqueTBDates.map(a => `'${a.getFullYear()}-${a.getMonth()+1}-${a.getDate()}'::date`).join(", ")
            ], r => r.rowCount)
        } catch (error)
        {
            x = 5;
        }
        const tbDataInsert = database.pgp.helpers.insert(tbDataInsertValues, tbDataCS);
        await database.db.none(tbDataInsert);

        input.channel.send(`TB data uploaded for ${uniqueTBDates.length} Territory Battles.  Replaced ${recsDeleted} existing records.`)
    },
    hotbotCodeToTbType(code)
    {
        switch (code)
        {
            case "t01D": return tbTypes.lshtb; break;
            case "t02D": return tbTypes.dshtb; break;
            case "t03D": return tbTypes.dsgtb; break;
            case "t04D": return tbTypes.lsgtb; break;
            default:
                return null;
        }
    },
    addManualTbData: async function(client, input, args)
    {
        // guild admin only
        let { botUser, guildId } = await this.checkPermissions(input) || {};
        if (guildId == null) return;

        // allycode tb_type yyyymmdd waves=67
        let spl = this.splitArgs(args);
        if (spl.length < 4)
        {
            input.channel.send("You must specify an allycode, a tb type, the date, and the data to set. For example:\r\n" +
                "guild.addmanualtbdata 111111111 LSGTB 20210730 waves=62");
            return;
        }

        let allycode;
        try {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(spl[0]);
        } catch (err)
        {
            input.channel.send(err);
            return;
        }

        let player = await database.db.oneOrNone("SELECT player_name FROM guild_players WHERE guild_id = $1 AND allycode = $2",
            [guildId, allycode]);
        if (!player)
        {
            input.channel.send(`Ally code ${allycode} does not appear to be part of your guild. If they just joined, try again tomorrow.`)
            return;
        }

        let { tbType, date } = this.parseTBTypeAndDate(input, spl[1], spl[2]) || {};
        // the parse function sends output to the channel, so no need to do so here
        if (tbType == null || date == null) return;

        const regex = /\s*(\w+)=(\d+)\s*/g

        
        var data = spl.slice(3).join(" ").trim();
        let dataMatch = regex.exec(data);
        
        if (dataMatch != null)
        {
            let responseString = "";
            do {
                var type = dataMatch[1];
                var value = parseInt(dataMatch[2]);

                if (dataTypes[type] == dataTypes.waves)
                {
                    if (value > tbType.maxWaves)
                    {
                        responseString += `:warning: Max wave count for ${tbType.key} is ${tbType.maxWaves}.  You specified ${value}.\r\n`
                        continue;
                    }

                    await database.db.any(
                        `INSERT INTO player_tb_data (allycode, tb_type, date, combat_waves, guild_id, manual_entry) 
                        VALUES ($1, $2, $3, $4, $5, $6)
                        ON CONFLICT (allycode, tb_type, date, guild_id, manual_entry)
                        DO UPDATE SET combat_waves = $4`,
                        [allycode, tbType.key, date, value, guildId, true]);

                    responseString += `:white_check_mark: Set ${tbType.key} combat waves for ${allycode} on ${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} to ${value}.\r\n`
                }
                else
                {
                    responseString += ":warning: Data type not found: " + type + "\r\n"
                }

            } while ((dataMatch = regex.exec(data)) != null)

            if (responseString != "")
            {
                input.channel.send(responseString);
                return;
            }
        } else {
            input.channel.send("Invalid data format.  Try **waves=62**, for example.")
            return;
        }
    },
    deleteManualTbData: async function(client, input, args)
    {
        let { botUser, guildId } = await this.checkPermissions(input) || {};
        if (guildId == null) return;

        // allycode tb_type yyyymmdd        
        let spl = this.splitArgs(args);
        if (spl.length < 3)
        {
            input.channel.send("You must specify an allycode, a tb type, and the date. For example:\r\n" +
                "guild.deletemanualtbdata 111111111 LSGTB 20210730");
            return;
        }

        let allycode;
        try {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(spl[0]);
        } catch (err)
        {
            input.channel.send(err);
            return;
        }

        let player = await database.db.oneOrNone("SELECT player_name FROM guild_players WHERE guild_id = $1 AND allycode = $2",
            [guildId, allycode]);
        if (!player)
        {
            input.channel.send(`Ally code ${allycode} does not appear to be part of your guild. If they just joined, try again tomorrow.`)
            return;
        }

        let { tbType, date } = this.parseTBTypeAndDate(input, spl[1], spl[2]) || {};
        // the parse function sends output to the channel, so no need to do so here
        if (tbType == null || date == null) return;

        var deleted = await database.db.result("DELETE FROM player_tb_data WHERE manual_entry=TRUE AND guild_id = $1 AND allycode = $2 AND tb_type = $3 AND date = $4",
            [guildId, allycode, tbType.key, date], r => r.rowCount)

        if (deleted)
        {
            input.channel.send(`Deleted manual data for ${allycode} for the ${tbType.key} on ${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}.`);
        }
        else
        {
            input.channel.send(`No data found for ${allycode} for the ${tbType.key} on ${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}.`)
        }
    },
    delete: async function(client, input, args)
    {

        let { botUser, guildId } = await this.checkPermissions(input) || {};
        if (guildId == null) return;

        let { tbType, date } = this.parseTBTypeAndDateFromArgs(input, args, 
            () => {
                input.channel.send("You have to specify a tb type and a date (yyyymmdd) that the data was recorded. For example:\r\n" + 
                "guild.deletetbdata dsgtb 20210730");
            }) || {};
        if (tbType == null || date == null) return;

        await database.db.none("DELETE FROM player_tb_data WHERE guild_id = $1 AND tb_type = $2 AND date = $3 AND manual_entry = FALSE",
            [guildId, tbType.key, date])

        input.channel.send(`All ${tbType.key} for your guild from ${date.toDateString()} has been deleted.`);
    },
    listTBData: async function(client, input, args)
    {
        let { botUser, guildId } = await this.checkPermissions(input) || {};
        if (guildId == null) return;

        const uploads = await database.db.any(
            `SELECT DISTINCT tb_type, date, count(combat_waves) count
            FROM player_tb_data
            WHERE guild_id = $1
            GROUP BY tb_type, date
            ORDER BY date ASC`, [guildId]);

        var embedToSend = new MessageEmbed().setTitle("TB Data Uploads").setColor(0x338811);
        var desc = "";
        for (var uploadIx = 0; uploadIx < uploads.length; uploadIx++)
        {
            let upload = uploads[uploadIx];
            desc += `${uploadIx+1}) ${upload.tb_type}, ${upload.date.getFullYear()}${(upload.date.getMonth()+1).toString().padStart(2, '0')}${upload.date.getDate().toString().padStart(2, '0')}: ${upload.count} ${upload.count == 1 ? "entry" : "entries"}\r\n`
        }
        embedToSend.setDescription(desc);
        input.channel.send(embedToSend);
    },
    checkPermissions: async function(input)
    {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        return { botUser, guildId }
    },
    splitArgs: function(args)
    {
        if (!args || args.length == 0)
        {
            return null;
        }

        return args[0].split(" ");

    },
    parseTBTypeAndDateFromArgs: function(input, args, failCallback)
    {
        let spl = this.splitArgs(args);

        if (spl == null || spl.length < 2)
        {
            failCallback();
            return null;
        }

        var typeString = spl[0].trim();
        var dateString = spl[1].trim();

        return this.parseTBTypeAndDate(input, typeString, dateString);

    },
    parseTBTypeAndDate: function(input, typeString, dateString)
    {

        var tbType = tbTypes[typeString];
        if (tbType == null)
        {
            input.channel.send(`Invalid TB Type "${typeString}".  Options are: ${Object.keys(tbTypes).join(", ")}`);
            return;
        }
        
        var dateRegex = /^(20[1-2][0-9])([0-1][0-9])([0-3][0-9])$/
        var dateMatch = dateRegex.exec(dateString);
        if (dateMatch == null)
        {
            input.channel.send(`Invalid date provided "${dateString}".  Format should be yyyymmdd (e.g., 20210730).`)
            return;
        }
        var date = new Date(dateMatch[1], parseInt(dateMatch[2])-1, dateMatch[3]);

        return {tbType, date};
    }
    
}