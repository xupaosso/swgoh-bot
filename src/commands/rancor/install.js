const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");
const rancorReset = require("./reset.js");
const database = require("../../database");

module.exports = {
    name: "rancor.install",
    aliases: ["shitpit.install"],
    description: "Install the WookieeBot Rancor/ShitPit monitor.",
    execute: async function(client, input, args, sfw) {
        if (!args || args.length == 0)
        {
            input.channel.send("Please provide a dedicated " + rancorUtils.getRancorOrShitPitWord(sfw) + " channel that I can read.");
            return;
        }

        let channelId = discordUtils.cleanDiscordId(args[0]);

        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        // check if the monitor is already installed in the channel
        var existingMonitor = await rancorUtils.getRancorMonitor(channelId);
        
        if (existingMonitor)
        {
            input.channel.send("Monitor is already installed in " + args[0] + ". If you need to reset it, use " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".reset.");
            return;
        }

        try {
            await client.channels.cache.get(channelId).send("Testing access.");
        } catch (error) {
            input.channel.send(args[0] + " is not a valid channel or I cannot read from it.  Please try again.");
            return;
        }
        
        await database.db.none("INSERT INTO rancor_monitor (channel) VALUES ($1)", [channelId]);

        input.channel.send(rancorUtils.getRancorOrShitPitWord(sfw) + " monitor installed in " + discordUtils.makeChannelTaggable(channelId));

        await rancorReset.resetMonitor(client, input, channelId, null, sfw);
    }
}
