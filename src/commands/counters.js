const { MessageEmbed } = require("discord.js");
const database = require("../database");
const guildUtils = require("../utils/guild");
const discordUtils = require("../utils/discord");
const playerUtils = require("../utils/player");
const countersAdd = require("./counters/add")
const countersDelete = require("./counters/delete")
const countersList = require("./counters/list")
const countersDesc = require("./counters/desc")
const counterUtils = require("./counters/counterUtils")

module.exports = {
    name: "counters",
    aliases: ["c"],
    description: "Guild-specific counter write-ups",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);

        let cmd = discordUtils.splitCommandInput(input);

        let subcommand = null;
        if (cmd.length >= 2) subcommand = cmd[1];

        switch (subcommand)
        {
            case "add":
                await countersAdd.execute(client, input, args);
                break;
            case "delete":
            case "del":
                await countersDelete.execute(client, input, args);
                break;
            case "desc":
                await countersDesc.execute(client, input, args);
                break;
            case "list":
                await countersList.execute(client, input, args);
                break;
            default:
                if (!args || args.length == 0)
                {
                    await countersList.execute(client, input, args);
                    break;
                }
                var counterName = args[0];
                counter = await this.findCounter(guildId, counterName);
                this.showCounter(input, counter) 
                break;
        }
    },
    findCounter: async function(guildId, counterName)
    {
        let r = await database.db.oneOrNone("SELECT name, short_desc, text FROM guild_writeup WHERE guild_id = $1 AND name = $2 AND type = $3", [guildId, counterName, counterUtils.counterWriteUpType])

        if (!r)
            return null;

        return {
            name: r.name,
            shortDescription: r.short_desc,
            text: r.text
        };
    },
    showCounter: function(input, counter)
    {
        if (!counter) {
            input.channel.send("Your guild does not have a counter by that name. Use c.list to see all your counters.");
            return;
        }

        var embedToSend = new MessageEmbed()
            .setTitle(counter.name)
            .setColor(0x347712)
            .setDescription("**" + counter.shortDescription + "**\r\n\r\n" + counter.text)

        input.channel.send(embedToSend);
    }
}