const data = require("../data/guides_data.json");
const { MessageEmbed } = require("discord.js");

module.exports = {
    name: "guide",
    description: "Selected Guides for SWGOH",
    list: function(message) {
        
        let list = Object.keys(data);

        let embed = new MessageEmbed()
            .setTitle("Available SWGOH Guides")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

        message.channel.send(embed);
    },
    execute: function(client, message, args) {
        if (args && args != "list")
        {
            var guide = args[0].trim();

            if (guide in data)
            {
                message.channel.send({
                    files: [data[guide]]
                });
                return;
            }
        }

        // they asked for the list or the requested guide wasn't found
        this.list(message);
    }
}