const { MessageEmbed } = require("discord.js");
const database = require("../../database");
const discordUtils = require("../../utils/discord")
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")

module.exports = {
    name: "counters.desc",
    aliases: ["c.desc"],
    description: "Add a guild-specific counter write-up",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        // main.js makes everything lowercase, so we're just redoing it without making it lowercase.
        // TODO: main.js toLowerCase() should be removed, but there's a lot that depends on it and I haven't had time to fix 
        let cmd = input.content.slice(discordUtils.prefix.length, input.content.indexOf(" "));
        let argStr = input.content.slice(discordUtils.prefix.length + cmd.length).trim();
        let regex = /(\w+)\s+([\s\S]+)/g

        let match = regex.exec(argStr);

        let name = match[1].trim();
        let text = match[2].trim();

        if (!name || !text)
        {
            input.channel.send(discordUtils.createErrorEmbed("Command Error", "You have to specify a one word keyword and a description (could be multiple lines). For example:\r\n" + 
            "^c.desc cls\r\n1. Check speeds\r\n2. Shoot Chewbacca\r\n3. Video: url"));
            return;
        }

        try {
            let r = await database.db.result(`
            UPDATE guild_writeup 
            SET text = $1
            WHERE guild_id = $2 AND name = $3 AND type = $4`, 
            [text, guildId, name, counterUtils.counterWriteUpType]);

            if (r.rowCount == 1)
            {
                input.channel.send("Guild write-up for " + name + " now has a full description of \"" + text + "\".")
                return;
            }
        } catch {
        }

        input.channel.send(discordUtils.createErrorEmbed("Something Went Wrong", "Please try again, or if it is not working, contact the bot owner."))
    }
}