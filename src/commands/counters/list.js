const { MessageEmbed } = require("discord.js");
const database = require("../../database");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")

module.exports = {
    name: "counters.list",
    aliases: ["c.list"],
    description: "List your guild-specific counter write-ups",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);

        const countersList = await database.db.any("SELECT name, short_desc FROM guild_writeup WHERE guild_id = $1 AND type = $2", [guildId, counterUtils.counterWriteUpType], r => r.counter_name)
        
        let listString;
        if (!countersList || countersList.length == 0)
            listString = "There are no counters defined for your guild."
        else
        {
            listString = " - " + countersList.sort(c => c.counter_name).map(c => "**" + c.name + "**: " + c.short_desc).join("\r\n - ")
        }
        
        let embed = new MessageEmbed()
            .setTitle("Available Counters")
            .setColor(0xD2691E)
            .setDescription(listString);

        input.channel.send(embed);
    }
}