const { MessageEmbed } = require("discord.js");
const database = require("../../database");
const discordUtils = require("../../utils/discord")
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")

module.exports = {
    name: "counters.delete",
    aliases: ["c.delete"],
    description: "Delete a guild-specific counter write-up",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }
        
        var counterName = args[0];
        var deleted = await database.db.any("DELETE FROM guild_writeup WHERE guild_id = $1 AND name = $2 AND type = $3",
            [guildId, counterName, counterUtils.counterWriteUpType], r => r.rowCount);

        if (deleted)
        {
            input.channel.send("Deleted your guild counter **" + counterName + "**.")
        }
        else
        {
            input.channel.send("Your guild does not have a counter by that name to delete.")
        }
    }
}