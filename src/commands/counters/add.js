const { MessageEmbed } = require("discord.js");
const database = require("../../database");
const discordUtils = require("../../utils/discord")
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")


module.exports = {
    name: "counters.add",
    aliases: ["c.add"],
    description: "Add a guild-specific counter write-up",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        // main.js makes everything lowercase, so we're just redoing it without making it lowercase.
        // TODO: main.js toLowerCase() should be removed, but there's a lot that depends on it and I haven't had time to fix 
        let cmd = input.content.slice(discordUtils.prefix.length, input.content.indexOf(" "));
        let argStr = input.content.slice(discordUtils.prefix.length + cmd.length).trim();
        let firstSpaceIx = argStr.indexOf(" ");
        let name = argStr.slice(0, firstSpaceIx);
        let desc = argStr.slice(firstSpaceIx).trim().replace(/(?:\r\n|\r|\n)/g, "");

        if (!name || !desc)
        {
            input.channel.send(discordUtils.createErrorEmbed("Command Error", "You have to specify a one word keyword and a short description. For example:\r\n" + 
            "^c.add cls CLS mirror match\r\n\r\nLong descriptions are added with **^c.desc**."));
            return;
        }

        try {
            let r = await database.db.result(`
            INSERT INTO guild_writeup (guild_id, name, short_desc, type) VALUES ($1, $2, $3, $4) 
            ON CONFLICT (guild_id,name,type)
            DO UPDATE SET short_desc = $3`, [guildId, name, desc, counterUtils.counterWriteUpType]);

            if (r.rowCount == 1)
            {
                input.channel.send("Guild write-up for " + name + " now has a short description of \"" + desc + "\".\r\nTo add a full description, use **c.desc " + name + " block of text**.")
                return;
            }
        } catch {}

        input.channel.send(discordUtils.createErrorEmbed("Something Went Wrong", "Please try again, or if it is not working, contact the bot owner."))
    }
}