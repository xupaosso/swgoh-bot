const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapiUtils = require("../../utils/swapi")

module.exports = {
    name: "tw.sendassignments",
    aliases: ["war.sendassignments"],
    description: "Send assignments to all players in your guild.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        
        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);
        const registeredUsers = await database.db.any("SELECT user_registration.allycode, user_registration.discord_id, guild_players.player_name FROM user_registration JOIN guild_players ON user_registration.allycode = guild_players.allycode WHERE guild_players.guild_id = $1", [guildId])

        if (!existingPlan || !existingPlan.result_json)
        {
            input.channel.send("It's total chaos out there - there is no plan!  Did you run one?");
            return;
        }

        var failures = "";
        var messagingPromises = new Array();

        for (var r in existingPlan.result_json)
        {
            var result = existingPlan.result_json[r];

            messagingPromises.push(
                this.sendOneAssignmentMessage(client, result, registeredUsers, existingPlan.teams_json)    
            );
        }
        let failureMessages = await Promise.all(messagingPromises);

        var failuresCount = 0;
        for (var msgIx in failureMessages)
        {
            var msg = failureMessages[msgIx];
            if (msg == null) continue;

            failures += msg + "\r\n"
            failuresCount++;
        }

        var messageToSend = "Sent assignments to " + (existingPlan.result_json.length - failuresCount) + " players."
        if (failuresCount > 0)
        {
            messageToSend += "\r\n\r\nThere were " + failuresCount + " failures:\r\n" + failures;
        }  
        
        let embedColor = failuresCount > 0 ? 0xaa0000 : 0x00aa00;
        embed = new MessageEmbed().setTitle("TW Assignments").setColor(embedColor).setDescription(messageToSend);
        input.channel.send(embed);
    },
    sendOneAssignmentMessage: async function(client, result, registeredUsers, teams)
    {
        var allycode = result.player.allycode;
        var registration = registeredUsers.find(u => u.allycode == allycode);
        if (registration == null)
        {
            return "No Discord ID: " + result.player.name + " (" + allycode + ")";
        }

        var discordId = registration.discord_id;

        var message = twUtils.getIndividualAssignmentMessage(result, teams);
        
        try {
            await (await client.users.fetch(discordId)).send(message);
        } catch (error)
        {
            return "Could not send: " + result.player.name + " (" + allycode + ") -- " + discordUtils.makeIdTaggable(discordId);
        }

        return null;
    }
}
