
const { createCanvas } = require("canvas");
const database = require("../../database");
const { MessageEmbed } = require("discord.js");
const statCalculator = require('swgoh-stat-calc');
const { unitImageCache } = require("../../utils/swapi");
const playerUtils = require("../../utils/player")

module.exports = {
    features : {
        options: [
            { 
                key: "z", 
                description: "Zetas", 
                formatString: function(c, v) { return `Zetas ${c} ${v}` },
                check: function(unit, comparison, checkValue) { 
                    var zetaCount = 0;
                    for (var skillIndex in unit.skills) {
                        if (unit.skills[skillIndex].isZeta && unit.skills[skillIndex].tier == unit.skills[skillIndex].tiers)
                            zetaCount++;
                    }
    
                    if (compare(zetaCount, comparison, checkValue)) 
                    {
                        return true;
                    }
                    return false;
                } 
            },
            { key: "g", description: "Gear Level", formatString: function(c, v) { return `Gear ${c} ${v}` }, check: function(unit, comparison, checkValue) { return unit.combatType != 1 || compare(unit.gear, comparison, checkValue); } },
            { key: "r", description: "Relic Level", formatString: function(c, v) { return `Relic ${c} ${v}` }, check: function(unit, comparison, checkValue) { return unit.combatType != 1 || compare(unit.relic.currentTier-2, comparison, checkValue); } },
            { key: "s", description: "Stars", formatString: function(c, v) { return `Stars ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.rarity, comparison, checkValue); } },
            { 
                key: "u", 
                description: "Has Ultimate (1=yes, 0=no)", 
                formatString: function(c, v) { return `Ultimate: ${v == 0 ? `No` : `Yes`}` }, 
                check: async function(unit, comparison, checkValue, playerData) {
                    let hasUlt = false;
                    if (playerData.ultData)
                    {
                        var ud = playerData.ultData.find(u => u.unit_base_id == unit.defId);
                        if (ud) hasUlt = ud.has_ult;
                    }
                    else
                    {
                        hasUlt = await database.db.oneOrNone("SELECT has_ult FROM player_ult WHERE allycode = $1 AND unit_base_id = $2", [playerData.allyCode.toString(), unit.defId])   
                    }

                    if (checkValue == 1 && !hasUlt) return false;
                    if (checkValue == 0 && hasUlt) return false;
                    return true;
                } 
            },
            { key: "speed", description: "Speed", formatString: function(c, v) { return `Speed ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final.Speed, comparison, checkValue); } },
            { key: "physical", description: "Physical Offence", formatString: function(c, v) { return `Phys. Off. ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["Physical Damage"], comparison, checkValue); } },
            { key: "special", description: "Special Offence", formatString: function(c, v) { return `Spec. Off. ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["Special Damage"], comparison, checkValue); } },
            { key: "critAvoidance", description: "Crit Avoidance", formatString: function(c, v) { return `CA ${c} ${v}%` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["Physical Critical Avoidance"]*100, comparison, checkValue); } },
            { key: "critDamage", description: "Crit Damage", formatString: function(c, v) { return `CD ${c} ${v}%` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["% Critical Damage"]*100, comparison, checkValue); } },
            { key: "critChance", description: "Crit Chance", formatString: function(c, v) { return `CC ${c} ${v}%` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["Physical Critical Chance"]*100, comparison, checkValue); } },
            { key: "tenacity", description: "Tenacity", formatString: function(c, v) { return `Tenacity ${c} ${v}%` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["% Tenacity"]*100, comparison, checkValue); } },
            { key: "potency", description: "Potency", formatString: function(c, v) { return `Potency ${c} ${v}%` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["% Potency"]*100, comparison, checkValue); } },
            { key: "armor", description: "Armor", formatString: function(c, v) { return `Armor ${c} ${v}%` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final.Armor*100, comparison, checkValue); } },
            { key: "health", description: "Health", formatString: function(c, v) { return `Health ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final.Health, comparison, checkValue); } },
            { key: "protection", description: "Protection", formatString: function(c, v) { return `Prot. ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final.Protection, comparison, checkValue); } },
            { key: "totalLife", description: "Total Life (Health + Protection)", formatString: function(c, v) { return `Total Life ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final.Health + unit.stats.final.Protection, comparison, checkValue); } },
            { key: "EHP", description: "EHP (Health + Protection) / (1-Armor)", formatString: function(c, v) { return `EHP ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(playerUtils.calculateEHP(unit.stats.final.Health, unit.stats.final.Protection, unit.stats.final.Armor), comparison, checkValue); } },
            { key: "accuracy", description: "Accuracy", formatString: function(c, v) { return `Acc. ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["Physical Accuracy"]*100, comparison, checkValue); } },
            { key: "resistance", description: "Resistance", formatString: function(c, v) { return `Resistance ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.stats.final["Resistance"]*100, comparison, checkValue); } },
            { key: "gp", description: "GP", formatString: function(c, v) { return `GP ${c} ${v}` }, check: function(unit, comparison, checkValue) { return compare(unit.gp, comparison, checkValue); } }
        ],
        nullFeature: { type: "n", comparison: "=", value: 0 },
        allKeys: function () { return this.options.map(o => o.key) }
    },
    isNullFeatureType: function(type)
    {
        return type == this.features.nullFeature.type;
    },
    getFeatureString: function(feature, comparison, value)
    {
        var feature = this.features.options.find(f => f.key.toLowerCase() == feature.toLowerCase());
        if (feature == null) return "";
        
        return feature.formatString(comparison, value);
    },
    isFleetTerritory : function(tIndex) { return tIndex == 7 || tIndex == 4; },
    calculateTotalPoints: function (numSquads, numFleets) {
        return numSquads * 30 + numFleets * 34;
    },
    getTerritoryNameByNumber: function(territoryNum)
    {
        return (territoryNum + 1).toString();
    },
    renderPlanResults: function(input, results, teams, territories)
    {

        const width = 6000;
        const height = 3400;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')
        const assignmentBgColor = "rgb(71, 184, 85)";
        const defaultFont = "24pt Arial";
        const defaultHeaderFont = "bold 30pt Arial"
        const defaultFontColor = "#000"

        context.fillStyle = '#ddd'
        context.fillRect(0, 0, width, height)


        var headerHeight = 100;
        var heightPerPlayer = (height - headerHeight)/results.length;
        var firstColWidth = 550; 
        var secondColWidth = 250;
        var firstAssignmentColumnX = firstColWidth + secondColWidth;
        var widthPerAssignmentColumn = (width - firstAssignmentColumnX)/10;
        var cellBuffer = 10;

        // draw alternative row colors and lines
        for (var resultsIndex = 0; resultsIndex < results.length; resultsIndex++)
        {
            // fill row background
            context.fillStyle = (resultsIndex %2 == 0 ? "rgb(210, 226, 240)" : "rgb(167,199,224)");
            context.fillRect(0, headerHeight + resultsIndex*heightPerPlayer, width, headerHeight + resultsIndex*(heightPerPlayer+1));

            // draw line
            context.fillStyle = "#000";
            context.beginPath();
            context.moveTo(0, headerHeight + resultsIndex*heightPerPlayer);
            context.lineTo(width, headerHeight + resultsIndex*heightPerPlayer);
            context.stroke();
        }

        // draw vertical lines
        var territoryCount = 0;
        context.fillStyle = "#000";
        context.beginPath();
        context.moveTo(firstColWidth, 0);
        context.lineTo(firstColWidth, height);
        context.stroke();
        for (var territoryIx in territories)
        {
            context.beginPath();
            context.moveTo(firstAssignmentColumnX + territoryCount*widthPerAssignmentColumn, 0);
            context.lineTo(firstAssignmentColumnX + territoryCount*widthPerAssignmentColumn, height);
            context.stroke();
            territoryCount++;
        }

        // write headers
        context.font = defaultHeaderFont;
        context.textAlign = "left"
        context.fillStyle = defaultFontColor;
        context.fillText("Player (Ally Code)", cellBuffer, headerHeight-cellBuffer);
        context.textAlign = "center";
        context.fillText("Points", firstColWidth + secondColWidth/2, headerHeight-cellBuffer);

        // write territory names and assignments count
        territoryCount = 0;
        for (var territoryIx = 0; territoryIx < territories.length; territoryIx++)
        {
            var territory = territories[territoryIx];
            
            context.textAlign = "center"
            context.fillText("Territory " + this.getTerritoryNameByNumber(territoryIx) + " (" + territory.curTeams + " teams)", firstAssignmentColumnX + (territoryCount+0.5)*(widthPerAssignmentColumn), headerHeight-cellBuffer, widthPerAssignmentColumn)
            territoryCount++;
        }

        results = results.sort((a, b) => { return a.player.name.normalize().localeCompare(b.player.name.normalize()) })
        for (var resultsIndex = 0; resultsIndex < results.length; resultsIndex++)
        {
            var result = results[resultsIndex];
            
            var totalPointsForPlayer = this.calculateTotalPoints(result.playerMetadata.numSquadsAssigned, result.playerMetadata.numFleetsAssigned);

            var rowTop = headerHeight + heightPerPlayer*resultsIndex;
            var rowTextYPos = rowTop + heightPerPlayer*0.5;
            
            context.font = defaultFont;
            context.textAlign = "left"
            context.fillStyle = defaultFontColor;
            context.fillText(result.player.name + " (" + result.player.allycode + ")", cellBuffer, rowTextYPos + cellBuffer, firstColWidth)
            context.textAlign = "center"
            context.fillText(totalPointsForPlayer, firstColWidth + secondColWidth/2, rowTextYPos + cellBuffer)

            for (var ta = 0; ta < result.territoryAssignments.length; ta++)
            {
                if (result.territoryAssignments[ta].foundTeamsCount == 0) 
                {
                    continue;
                }
                context.fillStyle = assignmentBgColor;
                context.fillRect(firstAssignmentColumnX + ta*widthPerAssignmentColumn+1, rowTop+1, widthPerAssignmentColumn-2, heightPerPlayer-2);

                var assignmentString = result.territoryAssignments[ta].foundTeamsCount + " (" + result.territoryAssignments[ta].foundTeamsString + ")";

                context.font = defaultFont;
                context.textAlign = "left"
                context.fillStyle = defaultFontColor;
                context.textAlign = "center"
                context.fillText(assignmentString, firstAssignmentColumnX + (ta+0.5)*(widthPerAssignmentColumn), rowTextYPos + cellBuffer)
            }
        }

        const buffer = canvas.toBuffer('image/png')
        
        input.channel.send({
            files: [{
                attachment: buffer,
                name: "tw-assignments-" + Date.now() + ".png"
            }]
        });
    },
    getIndividualAssignmentMessage: function(assignmentResult, teams)
    {
        var message = 
`**TW assignments for ${assignmentResult.player.name} (${assignmentResult.player.allycode})**
${assignmentResult.playerMetadata.numSquadsAssigned} squad${assignmentResult.playerMetadata.numSquadsAssigned == 1 ? "" : "s"} and ${assignmentResult.playerMetadata.numFleetsAssigned} fleet${assignmentResult.playerMetadata.numFleetsAssigned == 1 ? "" : "s"} (${this.calculateTotalPoints(assignmentResult.playerMetadata.numSquadsAssigned, assignmentResult.playerMetadata.numFleetsAssigned)} total points)\r\n`;
        for (var ta = 0; ta < assignmentResult.territoryAssignments.length; ta++)
        {
            var assignment = assignmentResult.territoryAssignments[ta];
            if (assignment.foundTeams.length == 0) continue;

            message += "\r\nTerritory " + this.getTerritoryNameByNumber(ta) + ":\r\n```"
            for (var teamIx = 0; teamIx < assignment.foundTeams.length; teamIx++)
            {
                var team = teams.find(t => t.name == assignment.foundTeams[teamIx]);
                message += team.name + ": " + team.description + "\r\n"
            }
            message += "```"
        }

        return message;
    },
    getExclusionsListEmbed: async function(guildId)
    {
        let currentExclusions = await database.db.any("SELECT tw_exclusion.allycode, guild_players.player_name FROM tw_exclusion LEFT JOIN guild_players USING (allycode) WHERE tw_exclusion.guild_id = $1", [guildId]);
        let message;
        if (currentExclusions.length == 0)
        {
            message = "No players are currently excluded from the war plan.";
        }
        else
        {
            message = "Players excluded from this war:\r\n - " + 
                currentExclusions.map(x => (x.player_name ? x.player_name : "Unknown Player ") + " (" + x.allycode + ")").join("\r\n - ");
        }
        message += "\r\n\r\nTo exclude a player from the war (e.g., they did not join), specify an ally code.  For example: **^tw.exclude 123456789**";
        message += "\r\n\r\nTo include a player in the war who has been excluded, specify an ally code.  For example: **^tw.include 123456789**";
        message += "\r\n\r\nTo reset your exclusions, use **^tw.include all**.";
        let embed = new MessageEmbed().setTitle("TW - Excluded Players").setDescription(message).setColor(0x349910)
        return embed;
    }
}


function compare(a, c, b)
{
    switch (c)
    {
        case '=': return a == b;
        case '<': return a < b;
        case '>': return a > b;
        case '<=': return a <= b;
        case '>=': return a >= b;
        default: return false;
    }
}