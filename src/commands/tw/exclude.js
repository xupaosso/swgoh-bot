const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.exclude",
    description: "Delete a team from the list of teams.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        if (!args || args.length == 0)
        {
            let embed = await twUtils.getExclusionsListEmbed(guildId);
            input.channel.send(embed);
            return;
        }

        try {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
        } 
        catch (error)
        {
            input.channel.send(error);
            return;
        }

        try {
            await database.db.any("INSERT INTO tw_exclusion (allycode, guild_id) VALUES ($1, $2)", [allycode, guildId]);
        } catch {
            input.channel.send("Ally code " + allycode + " has already been excluded.");
            return;
        }

        input.channel.send("Ally code " + allycode + " will be excluded from your war plan.")
    }
}
