const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapi = require("../../utils/swapi.js");

const numTerritories = 10;
const maxTeamPriority = 9;

module.exports = {
    name: "tw.runplan",
    aliases: ["war.runplan", "tw.run", "war.run"],
    description: "Create a plan for the war.",
    execute: async function (client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        const statusMessage = await input.channel.send("Running TW Planner.  This may take a few minutes, and this message will be updated periodically.")

        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        var playerAssignments = {};
        var teams = await this.getTeamsList(guildId);
        var players = new Array();
        var guildWarMetadata = await this.getGuildWarMetadata(guildId);
        var territories = await this.getTerritories(guildWarMetadata);
        var exclusions = await this.getExclusions(guildId);

        // results of a pass, so that we can do it more than once
        var results = [];
        var cacheTime = null;

        var participatingAllyCodes = guildPlayers.allycodes.filter(a => exclusions.indexOf(a) == -1);
        var metadata = await this.populatePlayerMetadata(participatingAllyCodes, guildWarMetadata);

        statusMessage.edit("Loaded all data. Calculating assignments.")
        for (var acIx in participatingAllyCodes) {
            var allycode = participatingAllyCodes[acIx];

            var player = { allycode: allycode };
            var playerMetadata = metadata[allycode];
            players.push(player);

            var playerData = null;
            var cache = await database.db.oneOrNone("SELECT data_json, timestamp, ult_data_json FROM player_cache WHERE allycode = $1 AND guild_id = $2", [allycode, guildId])
            if (cache)
            {
                playerData = cache.data_json;
                playerData.ultData = cache.ult_data_json;
                cacheTime = cache.timestamp;
            } 
            if (!playerData) 
            {
                try {

                    let payload = {
                        allycode: player.allycode,
                        language: "eng_us"
                    };
        
                    let fetchPlayer = await swapi.swapi.fetchPlayer(payload);
                    playerData = fetchPlayer.result[0];
                }
                catch (error) {
                    input.channel.send("Something went wrong getting a player: " + error);
                    return;
                }
            }

            // store for later, since we'll discard playerData when the iteration of the loop ends
            player.name = playerData.name;

            // viableTeams is a list of viable teams found for the player
            // unitsAssigned holds the full list of DefIds that have already been assigned for the player
            playerAssignments[player.allycode] =
            {
                viableTeams: [],
                unitsAssigned: []
            };

            await this.catalogViableTeams(player, playerData, playerAssignments, teams);

            // Loop through territories and figure out what to assign    
            var territoryAssignments = this.determineAssignments(player, playerAssignments, playerMetadata, territories, null, 0);

            results.push({
                player: player,
                playerMetadata: playerMetadata,
                territoryAssignments: territoryAssignments
            });

            if (results.length % 10 == 0)
                statusMessage.edit("Calculated " + results.length + "/" + participatingAllyCodes.length + " top priority assignments.");
        }

        statusMessage.edit("Calculating additional assignments.")
        for (var p = 1; p < maxTeamPriority; p++)
        {
            // additional passes at assignments of lower priorities
            this.doSecondaryAssignments(results, playerAssignments, territories, p);
        }

        await this.saveResults(guildId, results, teams, territories);

        var message = "Done planning. ";
        if (cacheTime)
            message += "Used cache from " + cacheTime.toString() + ". "
        message += "Here are your results. If all looks good, run ^tw.send. Otherwise, make changes and re-run. Good luck!";

        statusMessage.edit(message);

        twUtils.renderPlanResults(input, results, teams, territories);
    },
    getExclusions: async function(guildId)
    {
        return await database.db.map("SELECT allycode FROM tw_exclusion WHERE guild_id = $1", [guildId], r => r.allycode);
    },
    saveResults : async function(guildId, results, teams, territories)
    {
        var resultsText = JSON.stringify(results);
        var teamsText = JSON.stringify(teams);
        var territoriesText = JSON.stringify(territories);
        var existingResult = await database.db.oneOrNone("SELECT tw_plan_result_id FROM tw_plan_result WHERE guild_id = $1", [guildId]);
        if (existingResult)
            await database.db.any("UPDATE tw_plan_result SET result_json = $1, teams_json = $2, territories_json = $3 WHERE guild_id = $4", [resultsText, teamsText, territoriesText, guildId]);
        else
            await database.db.any("INSERT INTO tw_plan_result (guild_id, result_json, teams_json, territories_json) VALUES ($1, $2, $3, $4)", [guildId, resultsText, teamsText, territoriesText]);
        
        return;
    },
    getGuildWarMetadata: async function(guildId)
    {
        var metadata = {
            maxTeamsPerTerritory: null,
            defaultMaxSquadsPerPlayer: null,
            defaultMaxFleetsPerPlayer: null,
            guildId: guildId
        };
        var data = await database.db.any("select max_teams_per_territory,default_max_squads_per_player,default_max_fleets_per_player from tw_metadata WHERE guild_id = $1", [guildId]);
        if (data.length == 0)
        {
            data = await database.db.any("insert into tw_metadata (guild_id) values ($1) RETURNING *", [guildId]);
        }

        metadata.maxTeamsPerTerritory = data[0].max_teams_per_territory;
        metadata.defaultMaxFleetsPerPlayer = data[0].default_max_fleets_per_player;
        metadata.defaultMaxSquadsPerPlayer = data[0].default_max_squads_per_player;

        return metadata;
    },
    getTerritories: async function (guildWarMetadata) {
        var territories = new Array();
        // Capture territory metadata
        for (var i = 0; i < numTerritories; i++) {
            var territory = {};
            territory.territoryName = twUtils.getTerritoryNameByNumber(i);
            if (twUtils.isFleetTerritory(i)) { territory.isSquad = false; territory.isFleet = true; }
            else { territory.isSquad = true; territory.isFleet = false; }

            territory.maxTeams = guildWarMetadata.maxTeamsPerTerritory;
            territory.curTeams = 0; // will be incremented later during assignments phase

            territory.teamNamesToAssign = new Array();
            for (var p = 0; p < maxTeamPriority; p++) territory.teamNamesToAssign.push(new Array());

            territories.push(territory);
        }        
        
        // populate teams
        const plan = await database.db.any("SELECT * FROM tw_plan WHERE guild_id = $1", [guildWarMetadata.guildId]);
        for (var p in plan)
        {
            var planItem = plan[p];
            var teamName = planItem.team_name;
            var territoryIx = planItem.territory - 1;

            territories[territoryIx].teamNamesToAssign[planItem.priority-1].push(teamName);
        }

        return territories;
    },
    populatePlayerMetadata: async function (allycodes, guildWarMetadata) {
        var metadata = {};
        for (var acIx in allycodes) {
            var player = {};
            player.allycode = allycodes[acIx];
            player.maxSquads = guildWarMetadata.defaultMaxSquadsPerPlayer;
            player.maxFleets = guildWarMetadata.defaultMaxFleetsPerPlayer;
            player.numSquadsAssigned = 0;
            player.numFleetsAssigned = 0;

            metadata[player.allycode] = player;
        }
        return metadata;
    },
    getTeamsList: async function (guildId) {
        const teamsData = await database.db.any("select name,description,unit_def_id,feature,value,comparison,is_squad from tw_team join tw_team_unit on tw_team.tw_team_id = tw_team_unit.tw_team_id WHERE guild_id = $1 ORDER BY name ASC", [guildId]);
        return this.mapTeamsDataToObject(teamsData);
    },
    mapTeamsDataToObject: function(teamsData)
    {
        if (!teamsData) return null;
        
        var teams = new Array();
        var team, unit;
        for (var teamIx in teamsData) {
            var row = teamsData[teamIx];

            if (team == null || team.name != row.name) {
                team = { name: row.name, description: row.description, isSquad: row.is_squad, isFleet: !row.is_squad, units: new Array() };
                teams.push(team);
            }

            if (unit == null || unit.defId != row.unit_def_id) {
                unit = { defId: row.unit_def_id, features: new Array() };
                team.units.push(unit)
            }

            if (twUtils.features.options.find(f => f.key.toLowerCase() == row.feature.toLowerCase()))
            {
                unit.features.push({ key: row.feature, value: row.value, comparison: row.comparison })
            }
        }

        return teams;
    },
    isTerritoryFull: function (territory) {
        return territory.curTeams >= territory.maxTeams;
    },
    createAssignmentsString: function (territoryAssignment) {
        return (!territoryAssignment || territoryAssignment.foundTeamsCount == 0)
            ? ""
            : territoryAssignment.foundTeamsCount + " (" + territoryAssignment.foundTeamsString + ")";
    },
    catalogViableTeams: async function (player, playerData, playerAssignments, teams) {
        // loop through all teams.  First determine if the team is available to the player (units haven't already been assigned)
        // Then determine if the team is viable for the player (characters in roster, gear/zetas are sufficient)
        for (var teamIndex in teams) {
            var team = teams[teamIndex];

            if (await this.isTeamViable(playerData, team)) {
                // capture that the team is viable
                playerAssignments[player.allycode].viableTeams.push(team);

            }
        }
    },
    isTeamViable: async function (playerData, team) {
        var roster = playerData.roster;

        if (roster.length == 0) return false;
        if (!roster[0].stats)
            await playerUtils.calcRosterStats(roster);

        for (var teamUnitIndex in team.units) {
            teamUnit = team.units[teamUnitIndex];

            if (teamUnit.defId == "") {
                continue;
            }

            var ru = roster.filter(ru => ru.defId == teamUnit.defId);

            if (ru.length == 0) return false; // team can't be made because player doesn't have a team member

            for (var featureIx = 0; featureIx < teamUnit.features.length; featureIx++)
            {
                let feature = twUtils.features.options.find(f => f.key.toLowerCase() == teamUnit.features[featureIx].key.toLowerCase());
                if (!feature) continue;

                // hacky, but one off special case
                if (feature.key.toLowerCase() == "u")
                {
                    if (!await feature.check(ru[0], teamUnit.features[featureIx].comparison, teamUnit.features[featureIx].value, playerData)) return false;
                    continue;
                }

                if (!feature.check(ru[0], teamUnit.features[featureIx].comparison, teamUnit.features[featureIx].value)) return false;
            }
        }
        return true;
    },
    isTeamAvailableForPlayer: function (assignments, team) {
        for (var assignmentIndex in assignments) {
            for (var unitIndex in team.units) {
                if (team.units[unitIndex].defId == assignments[assignmentIndex])
                    return false;
            }
        }

        return true;
    },

    determineAssignments: function (player, playerAssignments, metadata, territories, territoryAssignments, passNum) {
        if (!territoryAssignments) {
            territoryAssignments = new Array(numTerritories);

            for (var i = 0; i < territoryAssignments.length; i++) {
                territoryAssignments[i] =
                {
                    foundTeamsCount: 0,
                    foundTeamsString: "",
                    foundTeamsDescriptionsString: "",
                    foundTeams: new Array()
                };
            }
        }

        // Loop through territories and figure out what to assign    
        for (var t = 0; t < numTerritories; t++) {
            // don't waste time if there are no viable teams left for the player
            if (playerAssignments[player.allycode].viableTeams.length == 0)
                break;

            territory = territories[t];

            // territory is full, so continue to the next
            if (this.isTerritoryFull(territory))
                continue;

            var foundTeamsCount = 0;
            var foundTeams = new Array();
            var foundTeamsString = "";
            var foundTeamsDescriptionsString = "";

            if (territoryAssignments[t]) {
                foundTeamsCount = territoryAssignments[t].foundTeamsCount;
                foundTeamsString = territoryAssignments[t].foundTeamsString;
                foundTeamsDescriptionsString = territoryAssignments[t].foundTeamsDescriptionsString;
                foundTeams = territoryAssignments[t].foundTeams;
            }

            var maxTeamsToAssign;

            maxTeamsToAssign = territory.teamNamesToAssign[passNum].length;

            // loop through names to assign for the territory, see what has a match in the player's viable teams
            for (var teamNameIndex = 0;
                teamNameIndex < maxTeamsToAssign && !this.isTerritoryFull(territory);
                teamNameIndex++) {

                var teamName = territory.teamNamesToAssign[passNum][teamNameIndex];

                var viableTeams = playerAssignments[player.allycode].viableTeams

                // loop through viableTeams in reverse so that we can remove teams when they are found via splice
                // If we want the logic to go forward, then we need to change it so that we decrease the index after we splice
                for (var viableTeamIndex = viableTeams.length - 1; viableTeamIndex >= 0 && !this.isTerritoryFull(territory); viableTeamIndex--) {

                    // if the player has used up all squad assignments, then don't both with other viable squad teams.  We don't want to break, though, in case we're looking at fleet teams.
                    if ((metadata.maxSquads == metadata.numSquadsAssigned) && viableTeams[viableTeamIndex].isSquad)
                        continue;

                    // if the player has used up all fleet assignments, then don't both with other viable fleet teams.  We don't want to break, though, in case we're looking at squad teams.
                    if ((metadata.maxFleets == metadata.numFleetsAssigned) && viableTeams[viableTeamIndex].isFleet)
                        continue;

                    // Look for a match with either displayName or name.  This allows us to specify a specific team composition (i.e., Revan (1)) or any composition of the team (i.e., Revan)
                    if (viableTeams[viableTeamIndex].displayName == teamName || viableTeams[viableTeamIndex].name == teamName) {
                        // found a match. 

                        // validate that the team is still available for the player based on what they've already been assigned
                        if (!this.isTeamAvailableForPlayer(playerAssignments[player.allycode].unitsAssigned, viableTeams[viableTeamIndex])) {
                            continue;
                        }
 
                        foundTeams.push(viableTeams[viableTeamIndex].name);

                        // Make it the assignment and remove from the player's viable teams
                        // we use the .name property so that we're not displaying (1), (2), etc. for each assignment.  It'd be confusing.
                        foundTeamsString += (foundTeamsString.length > 0 ? ", " : "") + viableTeams[viableTeamIndex].name;

                        // update the comment text with the team description.  Add newlines to separate teams
                        if (foundTeamsDescriptionsString.length > 0)
                            foundTeamsDescriptionsString += (String.fromCharCode(10) + String.fromCharCode(10));

                        foundTeamsDescriptionsString += (viableTeams[viableTeamIndex].name + ": " + viableTeams[viableTeamIndex].description);

 
                        territory.curTeams++;
                        foundTeamsCount++;

                        if (territory.isSquad) metadata.numSquadsAssigned++;
                        if (territory.isFleet) metadata.numFleetsAssigned++;

                        // Record that the units in the team have been assigned for the player
                        for (var unitIndex in viableTeams[viableTeamIndex].units) {
                            // Only add it if the unit isn't blank
                            if (viableTeams[viableTeamIndex].units[unitIndex].defId != "") {
                                playerAssignments[player.allycode].unitsAssigned.push(viableTeams[viableTeamIndex].units[unitIndex].defId);
                            }
                        }

                        viableTeams.splice(viableTeamIndex, 1);


                        break;
                    }
                }

            }

            territoryAssignments[t] =
            {
                foundTeamsCount: foundTeamsCount,
                foundTeams: foundTeams,
                foundTeamsString: foundTeamsString,
                foundTeamsDescriptionsString: foundTeamsDescriptionsString
            };

        }

        return territoryAssignments;
    },

    doSecondaryAssignments: function (previousResults, playerAssignments, territories, passNum) {
        for (var resultIndex in previousResults) {

            var result = previousResults[resultIndex];
            var player = result.player;
            var playerMetadata = result.playerMetadata;
            var territoryAssignments = result.territoryAssignments;

            // Loop through territories and figure out what to assign    
            this.determineAssignments(player, playerAssignments, playerMetadata, territories, territoryAssignments, passNum);
        }
    }
}


