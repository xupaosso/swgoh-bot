const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");

module.exports = {
    name: "tw.help",
    aliases: ["war.help"],
    description: "Help for the WookieeBot TW planner.",
    execute: async function(client, input, args) {

        let overviewField = {
            name: "Overview", 
            value:
`The WookieeBot TW Planner helps you create a plan and automatically assigns teams to qualified players based on the plan. Your guild must be registered with WookieeBot to use.

Key features:
   1. Define possible teams, including advanced filtering capabilities.
   2. Create a priority order of assignments.
   3. Limit the number of squads/fleets per player or teams per territory.
   4. Define which teams go in which territories.
   5. Fast!  Recalculate an entire plan in seconds.
   6. Direct discord messaging of assignments (requires one-time registration)
   7. Set custom descriptions for each team.
   8. On-demand graphical display of the plan and assignments.
   9. Roles & permissions to limit who can make changes to the plan or metadata.
  10. Exclude specific players from the plan (e.g., if they did not join the war)
  `,
inline: false
            };

        let adminCommandsField = {
            name: "Admin Commands",
            value:
`**^tw.addTeam** (**^tw.at**): add a team to your list of possible teams
**^tw.deleteTeam** (**^tw.dt**): delete a team from your list of teams
**^tw.listTeams** (**^tw.lt**): list your teams
**^tw.testTeam** (**^tw.tt**): see who in your guild meets a team's criteria
**^tw.setDesc** (**^tw.sd**): set description for a team
**^tw.showPlan** (**^tw.sp**): show your current tw plan
**^tw.setTeam** (**^tw.st**): assign/update a team in a territory
**^tw.removeTeam** (**^tw.rt**): remove a team from a territory
**^tw.exclude**: exclude an allycode in your guild from the plan
**^tw.include**: include an excluded allycode in the plan
**^tw.config**: manage tw planner configuration
**^tw.runPlan** (**^tw.rp**): run the plan
**^tw.last**: show the last results of runPlan 
**^tw.sendAssignments** (**^tw.send**): send DMs with assignments to guild members
`,
            inline: false
        };

        let allUserCommandsField = {
            name: "Guild Member Commands",
            value:
`**^tw.listTeams** (**^tw.lt**): list your teams
**^tw.showPlan** (**^tw.sp**): show your current tw plan
**^tw.last**: show the last results of runPlan 
**^tw.myAssignments** (**^tw.my**): show your personal assignments
`,
            inline: false
        };

        let embed = new MessageEmbed()
            .setTitle("WookieeBot TW Planner")
            .setColor(0xD2691E)
            .addFields(overviewField, adminCommandsField, allUserCommandsField);

        input.channel.send(embed);
    }

}
