const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapi = require("../../utils/swapi.js");

var numTerritories = 10;
var passTypes = {
    first: 1,
    second: 2
};

module.exports = {
    name: "tw.lastplan",
    aliases: ["war.lastplan", "tw.last", "war.last"],
    description: "Show the last results of executing a tw plan.",
    execute: async function (client, input, args) {

        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);

        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json, territories_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        if (!existingPlan || !existingPlan.result_json)
        {
            input.channel.send("You do not have an existing plan saved.  Use **tw.runplan** to generate one.");
            return;
        }

        twUtils.renderPlanResults(input, existingPlan.result_json, existingPlan.teams_json, existingPlan.territories_json);
    }
}