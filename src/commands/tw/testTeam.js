const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const twRunPlan = require("./runPlan")

module.exports = {
    name: "tw.testteam",
    aliases: ["tw.tt"],
    description: "Test who would match a team.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);

        if (!args || args.length == 0)
        {
            input.channel.send("You must specify a TW team name to test.  Use **tw.lt** to see your teams.");
            return;
        }

        let team = await this.findTeam(guildId, args[0]);

        if (!team) {
            input.channel.send(`No team found by the name **${args[0]}**.  Please compare to your list of teams: **tw.lt**`)
            return;
        }

        input.channel.send(`Checking your guild's roster to see who has a viable **${args[0]}** team.`)

        var viablePlayers = new Array();
        var cacheTime;
        
        var playersChecked = 0;

        let caches = await database.db.any(`
        SELECT data_json, timestamp, ult_data_json 
        FROM player_cache
        JOIN guild_players ON player_cache.allycode = guild_players.allycode 
        WHERE guild_players.guild_id = $1`, [guildId]);

        if (caches.length == 0)
        {
            input.channel.send(`Your guild's player data isn't cached.  Run **guild.cache**.`)
            return;
        }

        for (var cacheIndex in caches)
        {
            let cache = caches[cacheIndex];

            let playerData = cache.data_json;
            playerData.ultData = cache.ult_data_json;
            if (!cacheTime) cacheTime = cache.timestamp;
            
            if (await twRunPlan.isTeamViable(playerData, team))
                viablePlayers.push(" - " + playerData.name + " (" + playerData.allyCode + ")")

            playersChecked++;
        }

        if (viablePlayers.length == 0) {
            input.channel.send(`No one in your guild matches the criteria for **${args[0]}**.`)
            return;
        }

        let embed = new MessageEmbed()
            .setTitle(`${viablePlayers.length} players match team **${args[0]}**`)
            .setColor(0x33e844)
            .setDescription(
                viablePlayers.join("\r\n")
            )
            .setFooter(`Using cached data for ${playersChecked} players from ${cacheTime.toString()}`)
        input.channel.send(embed);
    },
    findTeam: async function(guildId, teamName)
    {
        const teamData = await database.db.any(
            `SELECT name,description,unit_def_id,feature,value,comparison,is_squad 
            FROM tw_team 
            JOIN tw_team_unit on tw_team.tw_team_id = tw_team_unit.tw_team_id 
            WHERE guild_id = $1 AND tw_team.name = $2
            ORDER BY name ASC`, [guildId, teamName]);

        let team = twRunPlan.mapTeamsDataToObject(teamData);
        return team ? team[0] : null;
    }
}
