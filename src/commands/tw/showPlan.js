const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { createCanvas } = require("canvas");

const tBoxBorderColor = 'rgb(21, 180, 255)'
const tBoxFillColor = 'rgba(0, 123, 183, 0.9)'

const drawingData = 
[
    { xPos: 1250, yPos: 50, width: 350, height: 550 },
    { xPos: 1250, yPos: 650, width: 350, height: 550 },
    { xPos: 850, yPos: 50, width: 350, height: 550 },
    { xPos: 850, yPos: 650, width: 350, height: 550 },
    { xPos: 450, yPos: 50, width: 350, height: 350 },
    { xPos: 450, yPos: 450, width: 350, height: 350 },
    { xPos: 450, yPos: 850, width: 350, height: 350 },
    { xPos: 50, yPos: 50, width: 350, height: 350 },
    { xPos: 50, yPos: 450, width: 350, height: 350 },
    { xPos: 50, yPos: 850, width: 350, height: 350 },
]

module.exports = {
    name: "tw.showplan",
    aliases: ["war.showplan", "tw.sp", "war.sp"],
    description: "Show the plan you've laid out for the war.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);

        var territories = await this.loadTerritories(guildId);
        var map = this.createMapImage(territories);

        input.channel.send({
            files: [{
                attachment: map,
                name: "tw-map-" + Date.now() + ".png"
            }]
        });

    },
    loadTerritories : async function(guildId){
        
        var territories = new Array(10);

        const plan = await database.db.any("SELECT territory, team_name, priority FROM tw_plan WHERE guild_id = $1 order by territory asc, priority asc", [guildId]);
        for (var p in plan)
        {
            var planItem = plan[p];
            var team = { name: planItem.team_name, priority: planItem.priority };

            var territoryIx = planItem.territory - 1;
            if (!territories[territoryIx]) territories[territoryIx] = new Array();
            territories[territoryIx].push(team);
        }
        return territories;
    },
    createMapImage: function(territories) {

        const width = 1650;
        const height = 1250;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')

        context.fillStyle = 'rgb(106,53,0)'

        context.fillRect(0, 0, width, height)

        for (var t = 0; t < drawingData.length; t++)
        {
            this.drawTerritory(context, drawingData[t].xPos, drawingData[t].yPos, drawingData[t].width, drawingData[t].height);

            
            var territoryName = twUtils.getTerritoryNameByNumber(t) + (twUtils.isFleetTerritory(t) ? " (Fleet)" : " (Squad)");
            context.fillStyle = "rgb(26, 51, 230)"
            context.fillRect(drawingData[t].xPos, drawingData[t].yPos, drawingData[t].width, 55);
            context.font = "30pt Arial"
            context.textAlign = "center"
            context.fillStyle = "#fff";
            context.fillText(territoryName, drawingData[t].xPos + (drawingData[t].width/2), drawingData[t].yPos + 40)

            let text = "";
            for (var team in territories[t])
            {
                text += territories[t][team].priority + ": " + territories[t][team].name + "\r\n";
            }

            context.font = "30pt Arial"
            context.textAlign = "left"
            context.fillStyle = "#fff"
            context.fillText(text, drawingData[t].xPos + 20, drawingData[t].yPos + 100);
        }
        const buffer = canvas.toBuffer('image/png')

        return buffer;
    },
    drawTerritory: function(context, xPos, yPos, width, height)
    {
        this.drawBorder(context, tBoxBorderColor, xPos, yPos, width, height);

        context.fillStyle = tBoxFillColor;
        context.fillRect(xPos, yPos, width, height);

    },
    drawBorder: function (context, color, xPos, yPos, width, height, thickness = 1)
    {
        context.fillStyle=color;
        context.fillRect(xPos - (thickness), yPos - (thickness), width + (thickness * 2), height + (thickness * 2));
    }
}
