const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.listteams",
    aliases: ["war.listteams", "tw.lt", "war.lt"],
    description: "List teams you've created for the war.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }


        var guildId = await guildUtils.getGuildIdByInput(input);

        if (args)
            this.renderListWithDetails(input, guildId, args);
        else
            this.renderListWithoutDetails(input, guildId)
    },
    renderListWithDetails: async function(input, guildId, args)
    {
        
        let teamsData;
        let teamName = args[0].trim();

        teamsData = await database.db.any(`
        SELECT name, description, tu.def_id_array, tu.feature_array, tu.value_array, tu.comparison_array
        FROM   tw_team tt
        JOIN 
        (SELECT tw_team_id, array_agg(unit_def_id) as def_id_array, array_agg(feature)::varchar[] as feature_array, array_agg(value) as value_array, array_agg(comparison) as comparison_array
            FROM   tw_team_unit
            GROUP BY tw_team_id
        ) tu USING (tw_team_id)
        WHERE tt.guild_id = $1 AND tt.name LIKE $2`
        , [guildId, "%" + teamName + "%"]);

        if (!teamsData || teamsData.length == 0)
        {
            input.channel.send(`No teams found matching or similar to ${teamName}.  Please double check your teams (tw.lt).`);
            return;
        }

        // add this back in when we figure out how to show the definition of a specific team
        var unitsList = await playerUtils.getUnitsList();

        var embedToSend = new MessageEmbed().setTitle("Your TW Teams").setColor(0x00aa00);
        for (var teamIx in teamsData)
        {
            var team = teamsData[teamIx];
            let output = "**Description**: " + (team.description ? team.description : "None.  Set with ^tw.sd.") + "\r\n"
                + "**Units**:\r\n";
            
            //var uniqueUnits = team.def_id_array.filter((x, i, a) => a.indexOf(x) == i);
            var units = new Array();
            for (var i = 0; i < team.def_id_array.length; i++)
            {
                let unit = units.find(u => u.baseId == team.def_id_array[i]);
                if (!unit) 
                {
                    unit = { baseId: team.def_id_array[i], features: new Array() };
                    units.push(unit);
                }
                if (twUtils.isNullFeatureType(team.feature_array[i]))
                    unit.features.push(twUtils.features.nullFeature);
                else 
                    unit.features.push({ type: team.feature_array[i], value: team.value_array[i], comparison: team.comparison_array[i] });
            }

            var unitStrings = units.map(unit => {
                let unitDef = unitsList.find(u => u.baseId == unit.baseId);
                let str = unitDef.nameKey;
                if (unit.features.length > 0 && unit.features[0] != twUtils.features.nullFeature)
                {
                    str += " (";
                    for (var f = 0; f < unit.features.length; f++)
                    {
                        str += twUtils.getFeatureString(unit.features[f].type, unit.features[f].comparison, unit.features[f].value)
                        if (f != unit.features.length - 1) str += ", "
                    }
                    str += ")";
                }
                return str;
            })
            for (let s = 0; s < unitStrings.length; s++)
            {
                output += (s+1) + ") " + unitStrings[s] + "\r\n";
            }

            embedToSend.fields.push({
                name: ">>>>  " + team.name + "  <<<<",
                value: output,
                inline: false
            })
        }
        
        input.channel.send(embedToSend);
    },
    renderListWithoutDetails: async function (input, guildId)
    {
        const teamsData = await database.db.any("SELECT name,description FROM tw_team WHERE guild_id = $1", [guildId]);

        var output = "List of Territory War teams:";
        for (var teamIx in teamsData)
        {
            var team = teamsData[teamIx];
            output += "\r\n**" + team.name + (team.description ? "**: " + team.description : "**");
        }
        
        input.channel.send(output);
    }
}
