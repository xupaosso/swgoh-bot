const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.removeteam",
    aliases: ["war.removeteam", "tw.rt", "war.rt"],
    description: "Remove a team from a territory.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }
        
        if (!args || args.length == 0)
        {
            input.channel.send("You have to specify a territory number (1-10) and team name. For example:\r\n" + 
            "tw.rt 1 cls");
            return;
        }

        var lowerArgs = args[0].trim().toLowerCase();
        var firstSpaceIx = lowerArgs.indexOf(" ");
        if (firstSpaceIx == -1)
        {
            input.channel.send("You have to specify a territory number (1-10) and team name. For example:\r\n" + 
            "tw.rt 1 cls");
            return;
        }

        var territoryNum = parseInt(lowerArgs.slice(0, firstSpaceIx));

        if (isNaN(territoryNum) || territoryNum < 1 || territoryNum > 10)
        {
            input.channel.send("Territory number must be between 1 and 10.");
            return;
        }
        
        var teamName = lowerArgs.slice(firstSpaceIx + 1).trim();

        var teamRemoved = await database.db.result("DELETE FROM tw_plan WHERE guild_id = $1 AND team_name = $2 AND territory = $3",
        [guildId, teamName, territoryNum], r => r.rowCount);
        
        if (teamRemoved)
            input.channel.send("Removed team '" + teamName + "' from territory " + territoryNum + ".");
        else   
            input.channel.send("No team by that name was assigned to territory " + territoryNum + ".");
            
    }
}
