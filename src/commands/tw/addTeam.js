const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.addteam",
    aliases: ["tw.at"],
    description: "Add a team to the list of teams.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        if (!args || args.length == 0)
        {
            var validFeaturesString = "Valid features:\r\n - " + twUtils.features.options.map(f => `${f.key}: ${f.description}`).join("\r\n - ")
            input.channel.send("You have to specify a team name, team members, and features. For example:\r\n" + 
                "tw.at glk jedi master kenobi(g=13, z=6); ki-adi mundi (r>4, z=1); ahsoka tano (g=13, z=1)\r\n" +
                "tw.at exec exec(s>6)\r\n" +
                "tw.at fast_dr darthrev(speed>340)\r\n\r\n" +
                validFeaturesString);
            
            return;
        }
        //var input = "teamname jedi master kenobi(g:13, z:6); ki-adi mundi (g:13, z:1); ahsoka tano (g:13, z:1)"


        var team = {
            id: null,
            name: null,
            combatType: 0,
            units: []
        }
        var squadRx = /([\w\-()'\s]+)\s*\(([<>=\w,\s]*)\)/g
        var featureRx = /\s*([\w]+)\s*(<|>|=|<=|>=)\s*([\w]+)\s*/g
        
        var inputContent = args[0].trim().toLowerCase();
        var firstSpace = inputContent.indexOf(" ");
        team.name = inputContent.slice(0, firstSpace);

        var existingTeam = await database.db.oneOrNone("SELECT name FROM tw_team WHERE guild_id = $1 AND name = $2", [guildId, team.name]);
        if (existingTeam)
        {
            input.channel.send("You already have a team by the name '" + team.name + "'.");
            return;
        }
        
        var unitsString = inputContent.slice(firstSpace).trim();
        
        let match = squadRx.exec(unitsString);

        const unitsList = await playerUtils.getUnitsList();
        
        do {
            var unit = { name: null, defId: null, features: [] };
            unit.name = match[1].trim();
            var cutUnitName = unit.name.replace(/\W/g, "");
            var exactMatch = false;

            unitMatches = unitsList.filter((u) => 
                {
                    if (exactMatch) return false;
                    var cutNameKey = u.nameKey.replace(/\W/g, "").toLowerCase();
                    
                    
                    if (cutNameKey == cutUnitName)
                    {
                        exactMatch = true;
                        return true;
                    }
                    return cutNameKey.indexOf(cutUnitName) >= 0;
                }
            );

            if (unitMatches.length > 1){
                if (exactMatch) {
                    unitMatches = unitMatches.filter((u) => { return u.nameKey.replace(/\W/g, "").toLowerCase() == cutUnitName });
                } else {
                    var errorString = "" + unitMatches.length + " possible matches found for \"" + unit.name + "\"";
                    if (unitMatches.length > 5) errorString += " First five:\r\n"
                    else errorString += ":\r\n"
                    for (var u = 0; u < 5 && u < unitMatches.length; u++)
                    {
                        errorString += unitMatches[u].nameKey + (u != 4 && u != unitMatches.length-1 ? ", " : "");
                    }
                    input.channel.send(errorString);
                    return;
                }
            } 
            else if (unitMatches.length == 0)
            {
                input.channel.send("No unit found matching '" + unit.name + "'.")
                return;
            }

            unit.name = unitMatches[0].nameKey;
            unit.defId = unitMatches[0].baseId;
            if (!team.combatType)
            {
                team.combatType = unitMatches[0].combatType;
            } 
            else if (team.combatType != unitMatches[0].combatType)
            {
                var combatTypeName = (team.combatType == 1) ? "character" : "ship";
                input.channel.send("You cannot mix character and fleet units in the same team. " + unit.name + " is not a " + combatTypeName + ".");
                return;
            }


            var features = match[2].trim();
            let featuresMatch = featureRx.exec(features);
            
            if (featuresMatch != null)
            {
                do {
                    var featureType = featuresMatch[1].trim();
                    var featureComparison = featuresMatch[2].trim();
                    var featureValue = featuresMatch[3].trim();

                    if (twUtils.features.options.find(o => o.key.toLowerCase() == featureType) == null) {
                        input.channel.send("Unit feature not found: " + featureType);
                        return;
                    }
                    
                    var feature = { type: featureType, value: featureValue, comparison: featureComparison };
                    unit.features.push(feature);

                } while ((featuresMatch = featureRx.exec(features)) != null)
            } else {
                unit.features.push(twUtils.features.nullFeature)
            }

            team.units.push(unit);

        } while ((match = squadRx.exec(unitsString)) != null)


        
        try {
            var isSquad = team.combatType == 1;

            await database.db.one("INSERT INTO tw_team (name, guild_id, is_squad) VALUES ($1, $2, $3) RETURNING tw_team_id",
                [team.name, guildId, isSquad], e => { team.id = e.tw_team_id });
        } catch {
            input.channel.send("You already have a team by the name '" + team.name + "'.");
            return;
        }

        const teamUnitCS = new database.pgp.helpers.ColumnSet(
            ['tw_team_id', 
            'unit_def_id', 
            'feature', 
            'value',
            'comparison'], 
            {table: 'tw_team_unit'});
        
        var teamUnitInsertValues = new Array();

        
        for (var unitIndex = 0; unitIndex < team.units.length; unitIndex++)
        {
            var ucur = team.units[unitIndex];
            for (var featureIndex = 0; featureIndex < ucur.features.length; featureIndex++)
            {
                teamUnitInsertValues.push(
                    {
                        tw_team_id: team.id,
                        unit_def_id: ucur.defId,
                        feature: ucur.features[featureIndex].type,
                        value: ucur.features[featureIndex].value,
                        comparison: ucur.features[featureIndex].comparison
                    }
                );
            }
        }
        
        const teamUnitInsert = database.pgp.helpers.insert(teamUnitInsertValues, teamUnitCS);
        
        await database.db.none(teamUnitInsert);

        input.channel.send("Added team '" + team.name + "' with " + teamUnitInsertValues.length + " features.");
    }
}
