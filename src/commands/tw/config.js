const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.config",
    aliases: ["war.config", "tw.config", "war.config"],
    description: "Configure the planner.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        if (!args || !args.length)
        {
            this.showCurrentConfig(input, guildId);
            return;
        }

        var splitArgs = args[0].toLowerCase().trim().split(/ +/);

        if (splitArgs.length < 2)
        {
            input.channel.send ("You need to specify a configuration and a value.");
            return;
        }

        var value = parseInt(splitArgs[1]);
        if (value == NaN || value < 0 || value > 50)
        {
            input.channel.send ("You did not specify a valid value. Try a number between 0 and 50.");
            return;
        }


        switch (splitArgs[0])
        {
            case "max_squads":
            case "ms":
                await this.setMaxSquads(input, guildId, value);
                break;
            case "max_fleets":
            case "mf":
                await this.setMaxFleets(input, guidlId, value);
                break;
            case "teams_per_territory":
            case "tpt":
                await this.setTeamsPerTerritory(input, guildId, value);
                break;
            default:
                input.channel.send ("You need to specify a valid configuration and a value.");
                return;

        }
    },
    setMaxSquads: async function(input, guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET default_max_squads_per_player = $1 WHERE guild_id = $2", [value, guildId]);
        input.channel.send("Max squads per player set to " + value + ".");
    },
    setMaxFleetse: async function(input, guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET default_max_fleets_per_player = $1 WHERE guild_id = $2", [value, guildId]);
        input.channel.send("Max fleets per player set to " + value + ".");
    },
    setTeamsPerTerritory: async function(input, guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET max_teams_per_territory = $1 WHERE guild_id = $2", [value, guildId]);
        input.channel.send("Max teams per territory set to " + value + ".");
    },
    showCurrentConfig: async function(input, guildId)
    {
        const config = await database.db.oneOrNone("SELECT * FROM tw_metadata WHERE guild_id = $1", [guildId]);
        if (!config)
        {
            input.channel.send("Your guild is not properly configured.  Ask the bot owner to fix this.");
            return;
        }
        
        input.channel.send("Current TW Planner Configuration Settings:\r\n" +
            "Max squads per player: " + config.default_max_squads_per_player + "\r\n" +
            "Max fleets per player: " + config.default_max_fleets_per_player + "\r\n" +
            "Max teams per territory: " + config.max_teams_per_territory
        );
    },
    showHelp: function(input)
    {
        var helpText = "Valid configuration settings:\r\n" +
            "max_squads (ms) -- set max squads to be assigned per player\r\n" +
            "max_fleets (mf) -- set max fleets to be assigned per player\r\n" +
            "teams_per_territory (tpt) -- set max teams to be assigned in each territory\r\n";

        input.channel.send(helpText);
        /*
            options:
                tw.config max_squads 5
                tw.config max_fleets 1
                tw.config teams_per_territory 25
        */
        
    }
}
