const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapiUtils = require("../../utils/swapi")

module.exports = {
    name: "tw.myassignments",
    aliases: ["tw.my"],
    description: "List teams you've created for the war.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);

        let allycode = null;
        if (args && args.length > 0)
        {
            try {
                allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
                allycode = guildPlayers.allycodes.find(c => c == allycode);
            } 
            catch (error)
            {
                input.channel.send(`${args[0]} is not a valid ally code. Either specify a valid ally code to see a guild mate's assignments, or use **tw.my** to see your own assignments.`);
                return;
            }
        }
        else
        {
            allycode = guildPlayers.allycodes.find(p => botUser.allycodes.find(a => p == a))
        }

        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);
        
        if (!existingPlan || !existingPlan.result_json)
        {
            input.channel.send("You do not have any assignments.  Did an officer run the plan?");
            return;
        }

        var my = existingPlan.result_json.find(p => p.player.allycode == allycode );
        var teams = existingPlan.teams_json;

        var message = twUtils.getIndividualAssignmentMessage(my, teams);
        
        input.channel.send(message);
    }
}
