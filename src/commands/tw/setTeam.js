const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.setteam",
    aliases: ["war.setteam", "tw.st", "war.st"],
    description: "Add a team to the list of teams.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }
        
        if (!args || args.length == 0)
        {
            input.channel.send("You have to specify a territory number (1-10), optionally a priority (1-9, default 1 - highest), and a valid team name. For example:\r\n" + 
            "tw.st 1 cls\r\n" + "tw.st 1.5 gg");
            return;
        }

        var lowerArgs = args[0].trim().toLowerCase();
        var firstSpaceIx = lowerArgs.indexOf(" ");

        if (firstSpaceIx < 0)
        {
            input.channel.send("You have to specify a territory number (1-10), optionally a priority (1-9, default 1 - highest), and a valid team name. For example:\r\n" + 
            "^tw.st 1 cls\r\n" + "^tw.st 1.5 gg");
            return;
        }
        var territoryNum = lowerArgs.slice(0, firstSpaceIx);
        var teamName = lowerArgs.slice(firstSpaceIx + 1).trim();
        var priority = 1;

        var decIx = territoryNum.indexOf(".");
        if (decIx != -1)
        {
            priority = parseInt(territoryNum.slice(decIx + 1));
            territoryNum = parseInt(territoryNum.slice(0, decIx));
        } else {
            territoryNum = parseInt(territoryNum);
        }

        if (territoryNum < 1 || territoryNum > 10)
        {
            input.channel.send("Territory number must be between 1 and 10.");
            return;
        }
        
        if (isNaN(priority) || priority < 1 || priority > 9)
        {
            input.channel.send("Priority must be between 1 and 9.");
            return;
        }

        var teamValid = await this.validateTeam(guildId, teamName, territoryNum);
        if (!teamValid)
        {
            input.channel.send(teamName + " is not a valid team for that territory.  Check that you spelled it right and that you're not mis-matching squad and fleet territories.");
            return;
        }

        const existingTeam = await database.db.oneOrNone("SELECT * FROM tw_plan WHERE guild_id = $1 AND team_name = $2 AND territory = $3",
            [guildId, teamName, territoryNum]);

        var feedbackText = "";
        if (!existingTeam)
        {
            await database.db.none("INSERT INTO tw_plan (guild_id, territory, team_name, priority) VALUES ($1, $2, $3, $4)",
            [guildId, territoryNum, teamName, priority])

            feedbackText = "Set team '" + teamName + "' in territory " + territoryNum + " with priority " + priority + ".";
        } else {
            await database.db.none("UPDATE tw_plan SET priority = $1 WHERE guild_id = $2 AND team_name = $3 AND territory = $4",
            [priority, guildId, teamName, territoryNum])
            feedbackText = "Updated team '" + teamName + "' in territory " + territoryNum + ". Set priority to " + priority + ".";
        }
        input.channel.send(feedbackText);
    },
    validateTeam: async function(guildId, teamName, territoryNum)
    {
        const team = await database.db.oneOrNone("SELECT is_squad FROM tw_team WHERE guild_id = $1 AND name = $2", [guildId, teamName])
        if (team == null) return false;

        if (team.is_squad && twUtils.isFleetTerritory(territoryNum-1))
            return false;
        if (!team.is_squad && !twUtils.isFleetTerritory(territoryNum-1))
            return false;
        return true;
    }
}
