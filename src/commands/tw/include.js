const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.include",
    description: "Include a player in the war.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        if (!args || args.length == 0)
        {
            let embed = await twUtils.getExclusionsListEmbed(guildId);
            input.channel.send(embed);
            return;
        }

        if (args[0].toLowerCase().trim() == "all")
        {
            await database.db.none("DELETE FROM tw_exclusion WHERE guild_id = $1", [guildId]);
            input.channel.send("All previously excluded players are now included.")
            return;
        }

        try {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
        } 
        catch (error)
        {
            input.channel.send(error);
            return;
        }

        try {
            var deletedCount = await database.db.result("DELETE FROM tw_exclusion WHERE allycode = $1 AND guild_id = $2", [allycode, guildId], r=>r.rowCount);
            if (deletedCount > 0)
                input.channel.send("Ally code " + allycode + " is now included in the war.")
            else
                input.channel.send("No effect. Ally code " + allycode + " was not excluded.")
        } catch {
            input.channel.send("Huac ooac. ('Uh oh.').  Something went wrong.  Try again?");
            return;
        }

    }
}
