const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.setdescription",
    aliases: ["war.setdescription", "tw.sd", "war.sd"],
    description: "Set a description for a team.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }
        
        if (!args || args.length == 0)
        {
            input.channel.send("You have to specify an existing team and a description. For example:\r\n" + 
            "^tw.sd cls CLS, Chewie, Han, C3PO, Chewpio\r\n");
            return;
        }

        var argsTrimmed = args[0].trim(); 

        var firstSpaceIx = argsTrimmed.indexOf(" ");

        if (firstSpaceIx < 0)
        {
            input.channel.send("You have to specify an existing team and a description. For example:\r\n" + 
            "^tw.sd cls CLS, Chewie, Han, C3PO, Chewpio\r\n");
            return;
        }

        var teamName = argsTrimmed.slice(0, firstSpaceIx).toLowerCase();
        var description = argsTrimmed.slice(firstSpaceIx + 1).trim();

        const { rowCount } = await database.db.result("UPDATE tw_team SET description = $1 WHERE guild_id = $2 AND name = $3", [description, guildId, teamName])

        if (rowCount >= 1)
            input.channel.send("Updated description for team " + teamName + ".");
        else
            input.channel.send("No team by the name " + teamName + ". Use ^tw.addteam to add one.");
    }
}
