const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "tw.deleteteam",
    aliases: ["war.deleteteam", "tw.dt", "war.dt"],
    description: "Delete a team from the list of teams.",
    execute: async function(client, input, args) {
        var botUser = await playerUtils.getUser(input.author.id);
        if (botUser == null || botUser.guilds.length == 0) {
            input.channel.send("You are not registered with WookieeBot or you do not have any guilds assigned in WookieeBot.");
            return;
        }

        var guildId = await guildUtils.getGuildIdByInput(input);
        var guildRoles = botUser.guilds.find(g => g.guildId == guildId);
        if (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin) {
            input.channel.send("You do not have permission to do this.");
            return;
        }

        if (!args || args.length == 0)
        {
            input.channel.send("You have to specify a team name.");
            return;
        }

        var teamNameToDelete = args[0].trim().toLowerCase();
        var teamId = null;
        await database.db.one("DELETE FROM tw_team WHERE name = $1 AND guild_id = $2 RETURNING tw_team_id", 
        [teamNameToDelete, guildId], e => { teamId = e.tw_team_id; }); 

        if (teamId == null) {
            input.channel.send("No team found by the name '" + teamNameToDelete + "'");
        } 
        else
        {
            var unitsDeleted = await database.db.result("DELETE FROM tw_team_unit WHERE tw_team_id = $1", [teamId], r => r.rowCount);
            input.channel.send("Deleted team '" + teamNameToDelete + "' and all " + unitsDeleted + " features of the team.");
        }

        return;
    }
}
