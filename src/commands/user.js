const discordUtils = require("../utils/discord.js");
const userRegister = require("./user/register.js");
const playerUtils = require("../utils/player")
const userAdmin = require("./user/admin");
const userMe = require("./user/me");
const { UserManager } = require("discord.js");

module.exports = {
    name: "user",
    description: "User registration",
    execute: async function(client, input, args) {
        let cmd = discordUtils.splitCommandInput(input);

        switch (cmd[1])
        {
            case "register":
            case "r":
                await userRegister.execute(client, input, args);
                break;
            case "admin":
                await userAdmin.execute(client, input, args);
                break;
            case "me":
                await userMe.execute(client, input, args);
                break;
            default: break;
        }
    }
}