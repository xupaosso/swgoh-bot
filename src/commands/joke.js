const { MessageEmbed } = require("discord.js");
const got = require("got");

module.exports = {
    name: "dadjoke",
    aliases: [ "dj" ],
    description: "WookieeBot joke generator",
    execute: async function(client, message, args) {
        const response = await got("https://icanhazdadjoke.com/").json();
        message.channel.send(response.joke);
    }
}