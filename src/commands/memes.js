const data = require("../data/memes_data.json");
const { MessageEmbed } = require("discord.js");

module.exports = {
    name: "memes",
    description: "WookieeBot meme generator",
    check: function(cmd){
        return cmd in data;
    },
    post: function(message, cmd){
        message.channel.send(data[cmd]);
    },
    list: function(message) {
        let list = Object.keys(data);

        let embed = new MessageEmbed()
            .setTitle("Wookieebot Memes")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

        message.channel.send(embed);
    },
    execute: function(client, message, args) {
        if (args == "list" || args == null) {
            this.list(message);
            return;
        }
        message.channel.send("_Command has an invalid parameter_");
    }
}