const { MessageEmbed } = require("discord.js");

module.exports = {
    name: "help",
    description: "WookieeBot Help",
    execute: function(client, input, args) {
        
        let inviteField = {
            name: "// invite //", 
            value: "**^invite**: Sends a direct message with a link to invite WookieeBot to your server.",
            inline: false
        };
    
    
        let cheatingField = {
            name: "// cheating //", 
            value: 
            "Evaluate the likelihood the player cheated (based on current roster) to acquire 7★ legendary/journey characters." + 
            "\r\n\r\n" +
            "  **^cheating allycode**: For those with a 20%+ likelihood of cheating, show a breakdown of which characters were likely used to acquire." +
            "\r\n" +  
            "  **^cheating allycode -d**: Show the breakdown of which characters were likely used for *all* legendary/journey characters." +
            "\r\n" +
            "  **^cheating.guild allycode**: Shows all players in the guild of the provided allycode that have a 60%+ likelihood of cheating at any checked events.",
            inline: false
        };
    
    
        let guidesField = {
            name: "// guide //",
            value:
                "**^guide**: Show a list of guides that can be provided." +
                "\r\n" +
                "Example: **^guide kam** (provides the KAM guide)" ,
            inline: false
        };
    
    
        let shitpitField = {
            name: "// Rancor (ShitPit) //",
            value:
                "**^rancor.help** or **^shitpit.help**: Shows help for the damage tracker for the Rancor (Challenge Tier) raid.",
            inline: false
        };
    
        let funField = {
            name: "// fun //",
            value: "**^fact**: Show a random fact about the Star Wars universe\r\n" +
                   "**^memes**: Show a list of memes that can be triggered after the ^ prefix\r\n" +
                   "**^dadjoke** or **^dj**: Tell a random dad joke.",
            inline: false
        }

        let embed = new MessageEmbed()
            .setTitle("WookieeBot Help")
            .setColor(0xD2691E)
            .addFields(inviteField, cheatingField, guidesField, shitpitField, funField);

        input.channel.send(embed);
    }
}

async function getHelp(args) {
    
    let helpEmbed = createEmbed();

    let inviteField = {
        name: "// invite //", 
        value: "**^invite**: Sends a direct message with a link to invite WookieeBot to your server.",
        inline: false
    };

    helpEmbed.fields.push(inviteField);

    let cheatingField = {
        name: "// cheating //", 
        value: 
        "Evaluate the likelihood the player cheated (based on current roster) to acquire 7★ legendary/journey characters." + 
        "\r\n\r\n" +
        "  **^cheating allycode**: For those with a 20%+ likelihood of cheating, show a breakdown of which characters were likely used to acquire." +
        "\r\n" +  
        "  **^cheating allycode -d**: Show the breakdown of which characters were likely used for *all* legendary/journey characters." +
        "\r\n" +
        "  **^cheating.guild allycode**: Shows all players in the guild of the provided allycode that have a 60%+ likelihood of cheating at any checked events.",
        inline: false
    };

    helpEmbed.fields.push(cheatingField);
    

    let guidesField = {
        name: "// guide //",
        value:
            "**^guide**: Show a list of guides that can be provided." +
            "\r\n" +
            "Example: **^guide kam** (provides the KAM guide)" ,
        inline: false
    };

    helpEmbed.fields.push(guidesField);
    

    let shitpitField = {
        name: "// Rancor (ShitPit) //",
        value:
            "**^rancor.help** or **^shitpit.help**: Shows help for the damage tracker for the Rancor (Challenge Tier) raid.",
        inline: false
    };

    helpEmbed.fields.push(shitpitField);

    let funField = {
        name: "// facts //",
        value: "**^fact**: Show a random fact about the Star Wars universe",
        inline: false
    }

    helpEmbed.fields.push(funField);
    
    // let troopersField = {
    //     name: "// imperial trooper speeds //", 
    //     value: 
    //     "Based on a provided speed for Colonel Starck or ally code, provide the ideal speeds for Death Trooper, Range Trooper, Snowtrooper, and General Veers." + 
    //     "\n\n" +
    //     "  **^troopers speed** or **^starck speed**: Show target speeds for the team. Ex.: ^troopers 280" + 
    //     "\n" + 
    //     "  **^troopers speed -e** or **^starck speed -e**: Show target speeds for the team, plus an explanation. Ex.: ^troopers 280 -e" + 
    //     "\n" +
    //     "  **^troopers allycode**: Show current and ideal speeds for the team. Ex.: ^troopers 123456789" + 
    //     "\n" + 
    //     "  **^troopers allycode -e** or **^starck allycode -e**: Show target speeds for the team, plus an explanation. Ex.: ^troopers 123-456-789 -e"
    //     ,
    //     inline: false
    // };

    // helpEmbed.fields.push(troopersField);

    let memesField = {
        name: "// memes generator //",
        value:
            "**^memes**: Show a list of memes that can be triggered after the ^ prefix",
        inline: false
    };

    helpEmbed.fields.push(memesField);

    return helpEmbed;
}