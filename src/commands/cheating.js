const database = require("../database");
const { MessageEmbed } = require("discord.js");
const playerUtils = require("../utils/player.js");
const guildUtils = require("../utils/guild.js");
const discordUtils = require("../utils/discord.js");
const logger = require('winston');
const cheatingGuild = require("./cheating/guild.js");
const cheatingUtils = require("./cheating/utils.js");
const cheating_data = require("../data/cheating_data.json");
const swapi = require("../utils/swapi.js");
const jp = require('jsonpath');

module.exports = {
    name: "cheating",
    description: "Check players for potential cheating.",
    execute: async function(client, input, args)
    {
        let cmd = discordUtils.splitCommandInput(input);

        if (cmd.length == 1) {
            await this.checkPlayer(client, input, args);
            return;
        }

        switch (cmd[1])
        {
            case "guild":
                await cheatingGuild.execute(client, input, args);
                break;
            default: break;
        }
    },
    checkPlayer: async function(client, input, args)
    {
        if (!client || !input) return;
        
        input.channel.send("Cheat check underway...");

        let resultsEmbed = await this.parsePlayer(input, args);
        if (resultsEmbed) await input.channel.send(resultsEmbed);
    },

    parsePlayer: async function(input, args) {
    
        if (!args || !args.length) {
            return cheatingUtils.createErrorEmbed("Rwww. ('No.').", "You didn't give me an ally code!");
        }
        
        var playerId = args[0].replace(/-/g, "");
        var detailed = false;
    
        if (args.length > 1)
        {
            for (var argIndex = 1; argIndex < args.length; argIndex++)
            {
                if (args[argIndex] == "d" || args[argIndex] == "details")
                {
                    detailed = true;
                }
            }
        }
    
        if (playerId.length != 9)
        {
            return cheatingUtils.createErrorEmbed("Rwww. ('No.').", "You gave me an invalid ally code (" + playerId + ")!");
        }
    
        let payload = {
            allycode:playerId, 
            language: "eng_us",
            project: {
                name: 1,
                roster: {
                    nameKey: 1,
                    rarity: 1,
                    gear: 1,
                    skills: 1
                }
            }
        };
    
        database.db.one("INSERT INTO cheating_queries(allycode, timestamp) VALUES($1, $2) RETURNING *",
                        [playerId, new Date()]);
        
        var unitsList = await playerUtils.getUnitsList();
    
        var player;
        try {
            let fetchPlayer = await swapi.swapi.fetchPlayer(payload);
            player = fetchPlayer.result;
        } 
        catch (error)
        {
            return cheatingUtils.createErrorEmbed("Huac ooac. ('Uh oh.').", error);
        }
    
        if (!player)
        {
            return cheatingUtils.createErrorEmbed("Aorcro rarrraahwh. ('Try again.')", "A player was not found with ally code (" + playerId + ").");
        }
        var playerName = player[0].name;
    
        var embedToSend = new MessageEmbed()
            .setTitle("Cheat check results for " + playerName + " (" + playerId + "):")
            .setColor(0xD2691E);
        
        for (var checkCharDisplayName in cheating_data)
        {
            var checkChar = cheating_data[checkCharDisplayName].character_name;
            var charResultsString = "";
            var fv = { name: null, value: null, inline: false };
    
            var rareCharQuery = jp.query(player, "$..roster[?(@.nameKey=='" + checkChar + "')]");
            
    
            fv.name = "__**" + checkCharDisplayName + "**__";
    
            if (rareCharQuery.length == 0)
            {
                charResultsString = "Not activated.";
                fv.name = ":zero: " + fv.name;
            } 
            else if (rareCharQuery[0].rarity < cheating_data[checkCharDisplayName].min_stars_to_check)
            {
                charResultsString = "Only checking units at " + cheating_data[checkCharDisplayName].min_stars_to_check + "★";
                fv.name = ":no_mouth: " + fv.name + " (" + rareCharQuery[0].rarity + "★)";
            }
            else
            {
    
                let results = await cheatingUtils.checkRequirements(player[0], cheating_data[checkCharDisplayName], unitsList);
    
                fv.name = (cheatingUtils.likelihoodSymbol(results.likelihood) + " " + fv.name + " (" + rareCharQuery[0].rarity + "★)").trim();
    
                var likelihoodString = results.likelihood + "% chance that " + playerName + " cheated.";
                
                charResultsString += likelihoodString;
                if (detailed || results.likelihood >= cheatingUtils.warningThreshold)
                {
                    charResultsString += "```";
                    for (var charIndex = 0; charIndex < results.characters.length; charIndex++)
                    {
                        var character = results.characters[charIndex];
                        charResultsString += character.characterName + ", " + 
                                            character.stars + "★, " + 
                                            "G" + character.gear + ", " + 
                                            character.zetas + " zetas\r\n";
                    }
                    charResultsString += "```";
                }
    
            }
    
            fv.value = charResultsString;
            
            embedToSend.addField(fv.name, fv.value, fv.inline);
        }
    
        return embedToSend;
    }
}