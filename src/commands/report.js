const { MessageEmbed } = require("discord.js");
const database = require("../database");
const guildUtils = require("../utils/guild.js");
const discordUtils = require("../utils/discord.js");
const playerUtils = require("../utils/player.js");
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const swapi = require("../utils/swapi.js");

const ONE_DAY_MILLIS = 1000*60*60*24;

const monthShortNames = 
[
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
];

const reportTypes = {
    "reek" : getReportReek,
    "gp" : getChartGpOverTimeForAllyCode,
    "gac" : getChartGacOverTimeForAllyCode,
    "gls" : getChartGLsForGuild,
    "mq" : getChartModQualityOverTimeForAllyCode,
    "payouts" : getChartGuildPayouts,
    "gg" : getChartGuildGrowth,
    "tb" : getChartTBPerformance
};

module.exports = {
    name: "report",
    description: "Generate Reports",
    list: function(input) {
        
        let list = Object.keys(reportTypes);

        let embed = new MessageEmbed()
            .setTitle("Available Reports")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

            input.channel.send(embed);
    },
    help: function(input) {
        let embed = new MessageEmbed()
            .setTitle("WookieeBot Reports")
            .setColor(0xD2691E)
            .setDescription(
                "gac: graph gac lifetime score for a player (30 days)\r\n" +
                "gls: graph GLs in the guild\r\n" +
                "gp: graph gp growth for a player (30 days)\r\n" +
                "gp.t: graph total gp growth for a player (30 days)\r\n" +
                "gp.s: graph squad gp growth for a player (30 days)\r\n" +
                "gp.f: graph fleet gp growth for a player (30 days)\r\n" +
                "mq: graph mod quality growth for a player (Wookiee style, 30 days)\r\n" +
                "mq.dsr: graph mod quality growth for a player (DSR style, 30 days)\r\n" +           
                "reek: list guild members that have not beaten the reek\r\n" + 
                "tb: combat waves over time\r\n" + 
                "payouts: chart of when guild members have payouts, and how many\r\n\r\n" + 

                "Examples:\r\n" +
                "^report reek 985736123\r\n" +
                "^report gp.s 985736123\r\n"
            );

            input.channel.send(embed);
    },
    execute: async function(client, input, args) {
        if (!args || !args.length)
        {
            this.help(input);
            return;
        }

        var splitArgs = args[0].toLowerCase().trim().split(/ +/);

        var allycode;

        if (splitArgs.length == 1)
        {
            // check if the caller is registered
            let user = await playerUtils.getUser(input.author.id);
            
            if (!user || !user.allycodes || user.allycodes.length == 0) 
            {
                this.sendError(input, "Error", "Need to be registered (use registeruser) or provide a report type and an ally code.");
                return;
            }

            var guildId = await guildUtils.getGuildIdByInput(input);
            var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);

            allycode = guildPlayers.allycodes.find(p => user.allycodes.find(a => p == a))

            if (!allycode)
            {
                this.sendError(input, "Error", "Need to be registered (use registeruser) or provide a report type and an ally code.");
                return;
            }

        } 
        else if (splitArgs.length != 2)
        {
            this.sendError(input, "Error", "Need to provide a report type and an ally code.");
            return;
        }
        else
        {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(splitArgs[1]);
        }
            
        var reportType = splitArgs[0].trim();

        var subreport = null;

        if (reportType.indexOf(".") > 0)
        {
            var splitReportType = reportType.split(/\./);
            if (splitReportType.length != 2)
            {
                this.sendError(input, "Invalid Subreport Format", "Format is [report type].[subreport]. For example: report gg.kam allycode");
                return;
            }
            reportType = splitReportType[0];
            subreport = splitReportType[1];
        }

        if (reportTypes[reportType]) {
            var reportObj;

            try {
                if (subreport)
                    reportObj = await reportTypes[reportType](allycode, subreport);
                else
                    reportObj = await reportTypes[reportType](allycode);
            } 
            catch (error)
            {
                this.sendError(input, error.message ? error.message : error);
                return;
            }

            if (reportObj.imageBuffer)
            {
                input.channel.send({
                    files: [{
                        attachment: reportObj.imageBuffer,
                        name: reportObj.fileName
                    }]
                });
                return;
            } else {
                input.channel.send(reportObj);
                return;
            }
        }

        this.list(input);
    },
    sendError: function(input, title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        input.channel.send(errorEmbed);
    }
}


function createImageReportResultObj(imageBuffer, fileName)
{
    return {
        imageBuffer: imageBuffer,
        fileName: fileName
    };
} 

async function getChartTBPerformance(allycode)
{
    const lineColorOptions = [
        'rgba(54, 162, 235, 0.5)',
        'rgba(235, 35, 235, 0.5)',
        'rgba(235, 35, 55, 0.5)',
        'rgba(40, 90, 19, 0.5)',
        'rgba(170, 150, 35, 0.5)',
        'rgba(50, 200, 100, 0.5)'
    ];
    const tbData = await database.db.any(
        `
        select distinct on (date) date, tb_type, combat_waves, player_name, manual_entry
        from player_tb_data
        left join player_data_static using (allycode)
        where allycode = $1
        group by date, manual_entry, tb_type, combat_waves, player_name
        order by date asc, manual_entry desc
        `, [allycode]
    )

    var labels = new Array();

    if (tbData.length == 0)
        throw `No data for allycode ${allycode}.`;

    const playerName = tbData[0].player_name; 
    var data = {};

    for (var i = 0; i < tbData.length; i++) {
        //var date = tbData[i].date;
        var label = tbData[i].date;
        if (labels.indexOf(label) == -1) labels.push(label);
        
        if (!data[tbData[i].tb_type]) data[tbData[i].tb_type] = new Array();
        
        data[tbData[i].tb_type].push({ x: label, y: tbData[i].combat_waves, id: `${i}${tbData[i].tb_type}`});
    }
    
    var max_entries_in_data = 0;
    const MAX_ENTRIES_TO_SHOW = 15;
    for (let type in data)
    {
        if (data[type].length > MAX_ENTRIES_TO_SHOW)
        {
            data[type] = data[type].slice(data[type].length-MAX_ENTRIES_TO_SHOW);
        }

        if (data[type].length > max_entries_in_data) max_entries_in_data = data[type].length;
    }

    const width = 1500;
    const height = 600;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Arial";
                ctx.textAlign = "center";

                var dsNum = 0;
                for (var t in data) {
                    for (var d in data[t])
                    {
                        var labelText = data[t][d].y;
                        var x = chartInstance.chart.controller.data.datasets[dsNum]._meta[0].data[d]._view.x;
                        var y = Math.min(chartInstance.chart.controller.data.datasets[dsNum]._meta[0].data[d]._view.y + 15, height - 120);
                        ctx.fillText(labelText, x, y);
                    }
                    dsNum++;
                }

            }
          });
    };

    var chartTitle = `TB Combat Waves for ${playerName} (${allycode})`;

    var configuration = {
        type: 'line',
        data: {
            datasets: new Array(),
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: chartTitle
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        displayFormats: {
                            month: "DD MMM YYYY",
                            quarter: "DD MMM YYYY",
                            day: "DD MMM YYYY",
                        }
                    },
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: max_entries_in_data
                    }
                }]
            }
        }
    };

    for (var type in data)
    {
        configuration.data.datasets.push({
            borderColor: lineColorOptions[configuration.data.datasets.length],
            backgroundColor: 'transparent',
            tension: 0,
            label: type,
            data: data[type]
        });
    }
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-tbcw-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGuildGrowth(allycode)
{
    var now = new Date();

    const ggData = await database.db.any(
        `select maxs.allycode, maxs.player_name, maxs.max_gac_lifetime_score, maxs.max_mod_quality2, maxs.max_gp_squad, maxs.max_gp_fleet, maxs.max_mods_speed25,
        mins.min_gac_lifetime_score, mins.min_mod_quality2, mins.min_gp_squad, mins.min_gp_fleet, mins.min_mods_speed25
  from 
    (select 
      player_data.allycode,
      player_data_static.player_name,
      max(player_data.gac_lifetime_score) max_gac_lifetime_score,
      max(player_data.mod_quality2) max_mod_quality2,
      max(player_data.gp_squad) max_gp_squad,
      max(player_data.gp_fleet) max_gp_fleet,
      max(player_data.mods_speed25) max_mods_speed25
  from player_data
  join player_data_static on player_data.allycode=player_data_static.allycode
  where timestamp between $1 and $2
  and player_data.allycode in (select allycode from guild_players where guild_id=(select guild_id from guild_players where allycode=$3))
  group by player_data.allycode,player_data_static.player_name) maxs
      join ( 
  select 
      distinct on (player_data.allycode) player_data.allycode,
      player_data_static.player_name,
      timestamp,
      gac_lifetime_score min_gac_lifetime_score,
      mod_quality2 min_mod_quality2,
      gp_squad min_gp_squad,
      gp_fleet min_gp_fleet,
      mods_speed25 min_mods_speed25
  from player_data
  join player_data_static on player_data.allycode=player_data_static.allycode
  join (select min(timestamp),allycode from player_data pdx group by allycode) pd2 on pd2.allycode = player_data.allycode
  where timestamp between $1 and $2
  and player_data.allycode in (select allycode from guild_players where guild_id=(select guild_id from guild_players where allycode=$3))
  order by player_data.allycode, player_data.timestamp asc) mins on maxs.allycode = mins.allycode
        `,
        [new Date(now-ONE_DAY_MILLIS*30).toDateString(), now.toDateString(), allycode]
    );

    var gains = {
        gacLifetime: [],
        modQuality: [],
        gpSquad: [],
        gpFleet: [],
        modsSpeed25: []
    }

    for (var d in ggData)
    {
        var gacLifetimeGain = ggData[d].max_gac_lifetime_score - ggData[d].min_gac_lifetime_score;
        var modQualityGain = (ggData[d].max_mod_quality2 - ggData[d].min_mod_quality2).toFixed(2);
        var gpSquadGain = ggData[d].max_gp_squad - ggData[d].min_gp_squad;
        var gpFleetGain = ggData[d].max_gp_fleet - ggData[d].min_gp_fleet;
        var modsSpeed25Gain = ggData[d].max_mods_speed25 - ggData[d].min_mods_speed25;

        if (gacLifetimeGain > 0)
            gains.gacLifetime.push({ gain: gacLifetimeGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (modQualityGain > 0)
            gains.modQuality.push({ gain: modQualityGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (gpSquadGain > 0)
            gains.gpSquad.push({ gain: gpSquadGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (gpFleetGain > 0)
            gains.gpFleet.push({ gain: gpFleetGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (modsSpeed25Gain > 0)
            gains.modsSpeed25.push({ gain: modsSpeed25Gain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });
    }

    gains.gacLifetime.sort((a,b) => b.gain - a.gain);
    gains.modQuality.sort((a,b) => b.gain - a.gain);
    gains.gpSquad.sort((a,b) => b.gain - a.gain);
    gains.gpFleet.sort((a,b) => b.gain - a.gain);
    gains.modsSpeed25.sort((a,b) => b.gain - a.gain);

    userGacLifetime = gains.gacLifetime.find(g => g.allycode == allycode);
    userModQuality = gains.modQuality.find(g => g.allycode == allycode);
    userGpSquad = gains.gpSquad.find(g => g.allycode == allycode);
    userGpFleet = gains.gpFleet.find(g => g.allycode == allycode);
    userModsSpeed25 = gains.modsSpeed25.find(g => g.allycode == allycode);

    let desc = "";

    desc += "GAC Lifetime Score Increase:\r\n";
    if (gains.gacLifetime.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.gacLifetime.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.gacLifetime[p].playerName + ": " + gains.gacLifetime[p].gain.toLocaleString("en-US") + "\r\n"
        }
    } else {
        desc += "No one gained any GAC Lifetime Score :cry:\r\n";
    }
    if (userGacLifetime)
        desc += "Your increase: " + userGacLifetime.gain.toLocaleString("en-US") + "\r\n";

    desc += "\r\nMod Quality Improvement:\r\n";
    
    if (gains.modQuality.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.modQuality.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.modQuality[p].playerName + ": " + gains.modQuality[p].gain + "\r\n"
        }
    } else {
        desc += "No one improved their mod quality :cry:\r\n";
    }
    if (userModQuality)
        desc += "Your improvement: " + userModQuality.gain + "\r\n";

    desc += "\r\nNew mods with 25+ speed secondary:\r\n";
    
    if (gains.modsSpeed25.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.modsSpeed25.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.modsSpeed25[p].playerName + ": " + gains.modsSpeed25[p].gain + "\r\n"
        }
    } else {
        desc += "No one got any new mods with 25+ speed secondary :cry:\r\n";
    }
    if (userModsSpeed25)
        desc += "Your new 25+ speed mods: " + userModsSpeed25.gain + "\r\n";

    desc += "\r\nSquad GP Growth:\r\n";
    
    if (gains.gpSquad.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.gpSquad.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.gpSquad[p].playerName + ": " + gains.gpSquad[p].gain.toLocaleString("en-US") + "\r\n"
        }
    } else {
        desc += "No one gained any squad GP :cry:\r\n";
    }
    if (userGpSquad)
        desc += "Your growth: " + userGpSquad.gain.toLocaleString("en-US") + "\r\n";

    desc += "\r\nFleet GP Growth:\r\n";
    
    if (gains.gpFleet.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.gpFleet.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.gpFleet[p].playerName + ": " + gains.gpFleet[p].gain.toLocaleString("en-US") + "\r\n"
        }
    } else {
        desc += "No one gained any Fleet GP :cry:\r\n";
    }
    if (userGpFleet)
        desc += "Your growth: " + userGpFleet.gain.toLocaleString("en-US") + "\r\n";

    var embed = new MessageEmbed()
        .setTitle("Guild Growth Achievements - Last 30 Days")
        .setColor(0xD2691E)
        .setDescription(desc);

    return embed;
}

async function getReportReek(allycode)
{
    
    let desc = "";
    var guildInfo = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    if (!guildInfo || !guildInfo.guildId || !guildInfo.allycodes || guildInfo.allycodes.length == 0)
    {
        desc = "Your guild is not registered."
    }
    else
    {
        const reekData = await database.db.any('SELECT allycode, player_name, reek_win FROM player_data_static WHERE reek_win = FALSE AND allycode LIKE ANY ($1) ORDER BY player_name ASC', [guildInfo.allycodes]);


        for (var d in reekData)
        {
            var dataRow = reekData[d];
            desc += dataRow.player_name + " (" + dataRow.allycode + ")\r\n";
        }

        if (desc == "")
            desc = "Congratulations!  All guild members have beaten the Reek.";
    }
    var embed = new MessageEmbed()
        .setTitle("Guild members that have not beaten the Reek mission")
        .setColor(0xD2691E)
        .setDescription(desc);

    
    return embed;
}

async function getChartGLsForGuild(allycode)    
{
    const glData = await database.db.any(
        `SELECT   
         COUNT(*) FILTER (WHERE gl_rey = TRUE) AS rey_count, 
         COUNT(*) FILTER (WHERE gl_slkr = TRUE) AS slkr_count,
         COUNT(*) FILTER (WHERE gl_jml = TRUE) AS jml_count,
         COUNT(*) FILTER (WHERE gl_see = TRUE) AS see_count,
         COUNT(*) FILTER (WHERE gl_jmk = TRUE) AS jmk_count,
         COUNT(*) AS players_count
         FROM player_data_static WHERE allycode LIKE ANY 
         (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))`, allycode
    );

    var data = [glData[0].rey_count, glData[0].slkr_count, glData[0].jml_count, glData[0].see_count, glData[0].jmk_count];

    const width = 600;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Roboto";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    var totalGls = (parseInt(glData[0].rey_count) + parseInt(glData[0].slkr_count) + parseInt(glData[0].jml_count) + parseInt(glData[0].see_count));
    var chartTitle = "Total GLs for guild: " + totalGls.toLocaleString('en');

    const configuration = {
        type: 'bar',
        data: {
            labels: ['Rey', 'SL Kylo Ren', 'JM Luke', 'Sith Emperor', 'JM Kenobi'],
            datasets: [{
                label: 'Count of Galactic Legends',
                data: data,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.7)',
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(255, 206, 86, 0.7)',
                    'rgba(75, 192, 192, 0.7)',
                    'rgba(163, 111, 187, 0.7)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(163, 111, 187, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },
            title: {
                display: true,
                align: 'center',
                text: chartTitle,
                position: 'bottom'
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gls-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGacOverTimeForAllyCode(allycode)
{
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;
    
    var gacAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/ONE_DAY_MILLIS);
        var gacChange = playerData[playerData.length-1].gac_lifetime_score - playerData[0].gac_lifetime_score;
        gacAveragePerDay = Math.round(gacChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        data.push(playerData[i].gac_lifetime_score);
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = "Average GAC Lifetime increase per day: " + gacAveragePerDay.toLocaleString('en');

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                cubicInterpolationMode: 'monotone',
                label: "GAC Lifetime Score over time for " + playerName + " (" + allycode + ")",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: chartTitle
            }
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gac-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGpOverTimeForAllyCode(allycode, type = null)
{
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var gpData = new Array();
    var gpSquadData = new Array();
    var gpFleetData = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;

    var gpAveragePerDay = "Not enough data.";
    var gpSquadAveragePerDay = "Not enough data.";
    var gpFleetAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/ONE_DAY_MILLIS);
        var gpChange = playerData[playerData.length-1].gp - playerData[0].gp;
        gpAveragePerDay = Math.round(gpChange / daysOfData);

        var gpSquadChange = playerData[playerData.length-1].gp_squad - playerData[0].gp_squad;
        gpSquadAveragePerDay = Math.round(gpSquadChange / daysOfData);   

        var gpFleetChange = playerData[playerData.length-1].gp_fleet - playerData[0].gp_fleet;
        gpFleetAveragePerDay = Math.round(gpFleetChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        gpData.push(playerData[i].gp);
        gpSquadData.push(playerData[i].gp_squad);
        gpFleetData.push(playerData[i].gp_fleet);
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var datasets = new Array();
    if (!type || type == "t")
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(54, 162, 235, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Total GP",
            data: gpData
        });
    }
    if (!type || type == "s")
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(14, 180, 5, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Squad GP",
            data: gpSquadData
        });
    }
    if (!type || type == "f") 
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(180, 25, 50, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Fleet GP",
            data: gpFleetData
        });
    }

    var line1 = "GP over time for " + playerName + " (" + allycode + ")";
    if (type == "s") line1 = "Squad " + line1; 
    if (type == "f") line1 = "Fleet " + line1; 

    var titleText = [
        line1,
        "Average GP increase per day: " + gpAveragePerDay.toLocaleString('en'),
        "Average Squad GP increase per day: " + gpSquadAveragePerDay.toLocaleString('en'),
        "Average Fleet GP increase per day: " + gpFleetAveragePerDay.toLocaleString('en')
    ];

    const configuration = {
        type: 'line',
        data: {
            datasets: datasets,
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: titleText
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gp-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartModQualityOverTimeForAllyCode(allycode, type = "")
{
    type = type ? type.toLowerCase() : "";
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        if (type == "dsr")
            data.push(playerData[i].mod_quality);
        else
            data.push(playerData[i].mod_quality2)
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = [
        "Mod Quality over time for " + playerName + " (" + allycode + "),"
    ];

    if (type == "dsr")
        chartTitle.push("calculated as (Number of +15 speeds) / (Squad GP / 100,000)");
    else
       chartTitle.push("calculated as (Num. 15+20+25 speeds + 0.5*Num. 6-dot) / (Squad GP / 100,000)");

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                cubicInterpolationMode: 'monotone',
                label: "Mod Quality",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: chartTitle
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-mq-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGuildPayouts(allycode, type = "")
{
    var guildName = null;
    var payoutsData = null;
    var guildData = await database.db.any("select guild_name from guild where guild_id = (select guild_id from guild_players where allycode = $1)", allycode);

    if (guildData && guildData[0]) {
        guildName = guildData[0].guild_name;
        var payoutsData = await database.db.any(
            `select distinct payout_utc_offset_minutes, count(payout_utc_offset_minutes)
            FROM player_data_static WHERE allycode LIKE ANY 
            (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))
            group by payout_utc_offset_minutes
            order by payout_utc_offset_minutes asc`, allycode
        );
    }

    if (payoutsData == null || payoutsData.length == 0)
    {
        // guild not registered in db, so load from API
        var guildData = await guildUtils.getGuildData(allycode);
        
        var memberAllyCodes = new Array();
        for (var m = 0; m < guildData.roster.length; m++)
        {
            guildName = guildData.name;
            var memberAllyCode = guildData.roster[m].allyCode;
            memberAllyCodes.push(memberAllyCode);
        }

        var playerPayouts = await swapi.fetchPlayer(
            { 
                allycodes: memberAllyCodes,
                project : 
                {
                    "poUTCOffsetMinutes": 1
                }
            }
        );

        playerPayouts.result.sort((a, b) => a.poUTCOffsetMinutes - b.poUTCOffsetMinutes);
        payoutsData = new Array();
        var lastPayoutData = null;
        for (var p = 0; p < playerPayouts.result.length; p++)
        {
            var poData = playerPayouts.result[p].poUTCOffsetMinutes;
            if (lastPayoutData == null || lastPayoutData.payout_utc_offset_minutes != poData)
            {
                lastPayoutData = { payout_utc_offset_minutes: poData, count: 1 }
                payoutsData.push(lastPayoutData);
            } else {
                lastPayoutData.count++;
            }
        }
    }

    var data = new Array();
    var labels = new Array();
    var backgroundColors = new Array();
    var borderColors = new Array();

    const colorLimits = {
        rMin: 20,
        rMax: 100,
        gMin: 0,
        gMax: 230,
        bMin: 100,
        bMax: 200
    }
    for (var i = 0; i < payoutsData.length; i++)
    {
        data.push(payoutsData[i].count);

        var offsetHours = payoutsData[i].payout_utc_offset_minutes / 60;
        var label = "UTC " + (offsetHours >= 0 ? "+" : "-") + Math.abs(offsetHours);
        labels.push(label);

        var baseColorR = Math.floor((colorLimits.rMax - colorLimits.rMin) / payoutsData.length) * i + colorLimits.rMin;
        var baseColorG = Math.floor((colorLimits.gMax - colorLimits.gMin) / payoutsData.length) * i + colorLimits.gMin;
        var baseColorB = Math.floor((colorLimits.bMax - colorLimits.bMin) / payoutsData.length) * i + colorLimits.bMin;
        backgroundColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 0.7)");
        borderColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 1)");
        
    }

    const width = 40*data.length;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Roboto";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    const configuration = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{

                label: 'Count',
                data: data,
                backgroundColor: backgroundColors,
                borderColor: borderColors,
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: "Payouts for " + guildName + ", grouped by time (UTC)"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-guild-payouts-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}