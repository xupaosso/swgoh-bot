var jp = require('jsonpath');
const statCalculator = require('swgoh-stat-calc');
const statNamingOption = require("../data/stats.json");
const swapi = require("../api/swgoh-help.js");

const explanation = 
"**Explanation:**\n" + 
"Starck uses Scan All Wavelengths, granting 20% turn meter to all. " +
"Death Trooper must go next to dispel any pretaunt (e.g., Zombie, KRU) " +
"or increase cooldowns (e.g., Bossk). You want range trooper immediately " +
"after (get your speed as close to the max above without going over) " + 
"to add more buffs and provide targeted damage against whoever you " +
"are killing first (e.g., weakest enemy).\n\n" +
"At this point, it's likely that your turn meter will be all out-of-whack " +
"because of buffs gained from assists (e.g., Veers).  If it's not, Snowtrooper " +
"can provide big targeted (or aoe) damage, and veers (speed as close to the number " +
"above without going over) can finish off or kill someone with his Ruthless Assault.\n";

const symbols = {
    ok: ":white_check_mark:",
    bad: ":warning:"
};

module.exports.getTrooperSpeeds = async function(args) {
    return await getTrooperSpeeds(args);
}

function getErrorEmbed(errorMessage)
{
    var embed = {};

    embed.title = "Error";
    embed.color = 0xaa0000;
    embed.description = errorMessage;
    return embed;
}

async function getTrooperSpeeds(args) {
    var starckSpeed = 0;
    
    var embed = {
        "title": "Target Speeds for Imperial Troopers\n" + 
                 "  (based on Colonel Starck speed)  ",
        "color": 0x2d4533,
        "description" : ""
    };

    if (!args.length) {
        return getErrorEmbed("Please provide either an ally code for the player to check or a speed for starck.");
    }

    var arg = args[0].replace(/-/g, "");
    var playerId = null;
    var starckSpeed = null;

    if (args[0].length >= 9)
    {
        playerId = args[0].replace(/-/g, "");

        if (playerId.length != 9)
            return getErrorEmbed("Please provide either an ally code for the player to check or a speed for starck.");
    } 
    else 
    {
        starckSpeed = parseInt(args[0]);

        if (isNaN(starckSpeed))
            return getErrorEmbed("Please provide a valid speed.");

        if (starckSpeed < 50)
            return getErrorEmbed("Speed specified is too low.  Try a speed above 50.");
    }

    var includeExplanation = false;
    console.log(args);
    if (args[1] && (args[1] == "e" || args[1] == "explain"))
    {
        includeExplanation = true;
    }

    if (playerId)
    {
        return await updateEmbedWithPlayerStarckSpeeds(embed, playerId, includeExplanation);
    }
    else
    {
        return await updateEmbedWithOfflineStarckSpeeds(embed, starckSpeed, includeExplanation);
    }
}

async function updateEmbedWithPlayerStarckSpeeds(embed, playerId, includeExplanation)
{
    
    const fetch = require('node-fetch');
    var fetchResult = await fetch('https://swgoh-stat-calc.glitch.me/gameData.json');
    let gameData = await fetchResult.json();
    statCalculator.setGameData( gameData );

    let payload = {
        allycode:playerId, 
        language: "eng_us" 
    };

    let { result, error, warning } = await swapi.fetchPlayer( payload );

    const player = result;
    var playerName = player[0].name;

    var line1 = "Imperial Troopers Target Speeds for " + playerName;
    var line2 = "(based on Colonel Starck speed)";

    var leadingSpacesCount = Math.floor((line1.length - line2.length) / 2 * 1.2);
    line2 = " ".repeat(leadingSpacesCount) + line2;

    embed.title = line1 + "\n" + line2;
    
    if (!player || !player[0] || !player[0].name) {
        return "No player found with allycode " + playerId + "\n" + JSON.stringify(player);
    }

    const starckDefId = "COLONELSTARCK";
    const deathTrooperDefId = "DEATHTROOPER";
    const rangeTrooperDefId = "RANGETROOPER";
    const veersDefId = "VEERS";
    const snowtrooperDefId = "SNOWTROOPER";
    
    var starckUnit = jp.query(player, "$..roster[?(@.defId=='" + starckDefId + "')]")[0];
    var deathTrooperUnit = jp.query(player, "$..roster[?(@.defId=='" + deathTrooperDefId + "')]")[0];
    var rangeTrooperUnit = jp.query(player, "$..roster[?(@.defId=='" + rangeTrooperDefId + "')]")[0];
    var snowtrooperUnit = jp.query(player, "$..roster[?(@.defId=='" + snowtrooperDefId + "')]")[0];
    var veersUnit = jp.query(player, "$..roster[?(@.defId=='" + veersDefId + "')]")[0];

    var gearCheck = 
        starckUnit.gear > 8 && deathTrooperUnit.gear > 8 && rangeTrooperUnit.gear > 8 && snowtrooperUnit.gear > 8 && veersUnit.gear > 8;
    if (!gearCheck)
    {
        embed.description += "Gear your Starck, Death Trooper, Range Trooper, Snowtrooper, and Veers to at least Gear 9, then let's talk :smile:";
        return embed;
    }

    var troopers = [starckUnit, deathTrooperUnit, rangeTrooperUnit, snowtrooperUnit, veersUnit];

    let count = statCalculator.calcRosterStats(troopers, { gameStyle: true, language: statNamingOption });

    starckSpeed = starckUnit.stats.final["Speed"];
    deathTrooperSpeed = deathTrooperUnit.stats.final["Speed"];
    rangeTrooperSpeed = rangeTrooperUnit.stats.final["Speed"];
    snowtrooperSpeed = snowtrooperUnit.stats.final["Speed"];
    veersSpeed = veersUnit.stats.final["Speed"];

    var targetSpeeds = calcTargetSpeeds(starckSpeed);
    
    var summary = "**Recommendations Summary:**\n";
    var findings = false;

    var starckGood = starckSpeed >= 200;
    if (!starckGood)
    {
        findings = true;

        summary += "Everyone's Starck should always be faster, but yours is too slow to be useful. " + 
        "This message will go aways when Starck is at least 200 speed, but to be broadly useful against teams like " +
        "Bounty Hunters, First Order, Ewoks, and Nightsisters, Starck should be at least 260 speed."; 
    }

    var deathGood = deathTrooperSpeed >= targetSpeeds.deathTrooperMin;
    if (!deathGood)
    {
        if (findings) summary += "\n\n";
        findings = true;

        summary += "Your death trooper is too slow. " + 
        "Increase his speed to at least " + targetSpeeds.deathTrooperMin + ". " + 
        "If your death trooper is too slow, then your opponent may get a turn between when Starck uses " + 
        "Scan All Wavelengths and when your death trooper goes to reduce cooldowns / remove buffs.";
    }


    var rangeGood = rangeTrooperSpeed == targetSpeeds.rangeTrooperMax;
    if (!rangeGood)
    {
        if (findings) summary += "\n\n";
        findings = true;

        if (rangeTrooperSpeed > targetSpeeds.rangeTrooperMax)
            summary += "Your range trooper is too fast.  Aim for exactly " + targetSpeeds.rangeTrooperMax + " speed. " + 
            "If your range trooper is any faster, he could take a turn before death trooper, which may waste range trooper's " + 
            "special against a tanky pretaunter and/or cause other troopers such as snowtrooper and veers to go too early.";
        
        if (rangeTrooperSpeed < targetSpeeds.rangeTrooperMax)
            summary += "Your range trooper is too slow.  Aim for exactly " + targetSpeeds.rangeTrooperMax + " speed. " + 
            "If your range trooper is less than the target speed, then there will be more opportunity for your enemy to take a " +
            "turn after your death trooper and before your range trooper.";

    }

    var snowtrooperGood = snowtrooperSpeed >= targetSpeeds.snowtrooperMin && snowtrooperSpeed <= targetSpeeds.snowtrooperMax;
    if (!snowtrooperGood)
    {
        if (findings) summary += "\n\n";
        findings = true;

        if (snowtrooperSpeed < targetSpeeds.snowtrooperMin)
        {
            summary += "Your snow trooper is too slow.  Aim for at least " + targetSpeeds.snowtrooperMin + " speed and " +
            "no more than " + targetSpeeds.snowtrooperMax + ". This will allow your snow trooper to be at 100% turn meter after " +
            "range trooper goes.";
        } 
        else 
        {
            summary += "Your snow trooper is too fast.  Aim for less than " + targetSpeeds.snowtrooperMax + " speed and " +
            "at least " + targetSpeeds.snowtrooperMin + ". This will allow your snow trooper to be at 100% turn meter after " +
            "range trooper goes, but not before.";
        }
    }
    
    var veersGood = veersSpeed == targetSpeeds.veers;
    if (!veersGood)
    {
        if (findings) summary += "\n\n";
        findings = true;

        if (veersSpeed > targetSpeeds.veers)
            summary += "Your Veers is too fast.  Aim for exactly " + targetSpeeds.veers + " speed. " + 
            "If your Veers is faster, then it is more likely that he will take a turn before snow trooper.  His " + 
            "aoe can trigger many passive abilities from enemies that could disrupt your train, so it is best " +
            "for him to go last if possible (but not so slow that enemies will have many opportunities for turns)."
            
        if (veersSpeed < targetSpeeds.veers)
            summary += "Your Veers is too slow.  Aim for exactly " + targetSpeeds.veers + " speed. " + 
            "If your Veers is less than the target speed, then there will be more opportunity for your enemy to take a " +
            "turn after your snowtrooper and before your Veers.";
    }


    embed.description += "Colonel Starck: (yours) " + starckSpeed + " " + (starckGood ? symbols.ok : ("; (ideal) *faster* " + symbols.bad)) + "\n";
    embed.description += "Death Trooper: (yours) " + deathTrooperSpeed + "; (ideal) " + targetSpeeds.deathTrooperMin + " (min) " + (deathGood ? symbols.ok : symbols.bad) + "\n";
    embed.description += "Range Trooper: (yours) " + rangeTrooperSpeed + "; (ideal) " + targetSpeeds.rangeTrooperMax  + " (max, target) " + (rangeGood ? symbols.ok : symbols.bad) + "\n";
    embed.description += "Snowtrooper: (yours) " + snowtrooperSpeed + "; (ideal) " + targetSpeeds.snowtrooperMin + " (min) - " + targetSpeeds.snowtrooperMax + " (max) " + (snowtrooperGood ? symbols.ok : symbols.bad) + "\n";
    embed.description += "General Veers: (yours) " + veersSpeed + "; (ideal) " + targetSpeeds.veers + " (max, target) " + (veersGood ? symbols.ok : symbols.bad) + "\n";

    if (!findings)
        summary = "Congratulations, your troopers are well-timed!  Check again when you increase Starck's speed :wink:";

    embed.description += "\n" + summary;
    
    if (includeExplanation)
    {
        embed.description += "\n\n" + explanation;
    }

    return embed;
}

function calcTargetSpeeds(starckSpeed)
{
    var targetSpeeds = { starck: starckSpeed };
    
    targetSpeeds.deathTrooperMin = Math.ceil((starckSpeed + 20) * 0.8) - 20;
    targetSpeeds.rangeTrooperMax = Math.ceil((starckSpeed + 20) * 0.8 - 1) - 20;
    targetSpeeds.snowtrooperMin = Math.ceil((targetSpeeds.rangeTrooperMax + 20) * 0.8) - 20;
    targetSpeeds.snowtrooperMax = targetSpeeds.rangeTrooperMax - 1;
    targetSpeeds.veers = Math.ceil((targetSpeeds.rangeTrooperMax + 20) * 0.8 - 1) - 20;
    return targetSpeeds;
}

async function updateEmbedWithOfflineStarckSpeeds(embed, starckSpeed, includeExplanation)
{
    var targetSpeeds = calcTargetSpeeds(starckSpeed);
    
    embed.description += "Colonel Starck: " + targetSpeeds.starck + "\n";
    embed.description += "Death Trooper: " + targetSpeeds.deathTrooperMin + " (min)\n";
    embed.description += "Range Trooper: " + targetSpeeds.rangeTrooperMax + " (max, target)\n";
    embed.description += "Snowtrooper: " + targetSpeeds.snowtrooperMin + " (min) - " + targetSpeeds.snowtrooperMax + " (max)\n";
    embed.description += "General Veers: " + targetSpeeds.veers + " (max, target)\n";

    if (includeExplanation)
    {
        embed.description += "\n" + explanation;
    }

    return embed;
}