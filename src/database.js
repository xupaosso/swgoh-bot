
// configure database connection
var logger = require('winston');

const initOptions = {
    // initialization options;
};

const dbConfig = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": process.env.DB_NAME,
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "ssl": process.env.DB_SSL
}

const pgp = require('pg-promise')(initOptions);

const db = pgp(dbConfig);

db.query("SELECT NOW() as now").then(data => { logger.info("Testing database connection. Now is: " + data[0].now); });

exports.db = db;
exports.pgp = pgp;